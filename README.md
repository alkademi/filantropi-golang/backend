# IF3250_2022_04_FILANTROPI_BACKEND

## Description

Backend service for Filantropi.id

## Requirement

- Golang 1.17
- Postgres 14
- Docker

## Setup
For developing process, you can ignore Docker setup.

### Golang

1. Change the name of environment file `.example.env` to `.env` 
2. In the root project folder, install dependencies

    ```
    go mod vendor
    go mod tidy
    ```

3. Run the server

   ```
    go run server.go
   ```

4. (Optional) Using air

- Install [air](https://github.com/cosmtrek/air)
- Run as following

    ```
    air server.go
    ```

### Docker (Optional)

1. Install Docker
2. Create a new Docker Network

    ```
    docker network create filantropi_network
    ```

3. Run the docker-compose

    ```
    docker-compose -p "filantropi-backend" -f "./deployments/dev/docker-compose.yml" up --build
    ```
   
## Executing

1. Open the url `http://localhost:8000/` and see if there is any response


## Documentation

1. After setup and executing golang, open the url `http://localhost:8000/docs`