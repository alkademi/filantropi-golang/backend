package main

import (
	"filantropi/configs"
	database "filantropi/internal/databases"
	"filantropi/internal/databases/seeds"
	"filantropi/internal/handlers"
	"filantropi/internal/routes"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
)

func main() {
	// Load the .env file.
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Load the server port.
	port := os.Getenv("SERVER_PORT")

	// Connect to the database.
	database.Connect()

	// Populate the database.
	seeds.Populate()

	// Initialize gin.
	r := configs.InitGin()

	// Gin general middleware.
	// CORS.
	r.Use(cors.New(configs.InitCors()))

	// Example of route.
	r.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/docs")
	})

	fileHandler := handlers.FileHandler{}
	// Api route.
	api := r.Group("/api/v1")
	{
		routes.User(api)
		routes.Fundraising(api)
		routes.Auth(api)
		routes.Files(api)
		routes.Donation(api)
		routes.Webhook(api)
		routes.Test(api)
	}

	// Swagger Documentation.
	r.Static("/docs", "./docs/swaggerui")
	r.StaticFile("/swagger", "./docs/swagger.yaml")

	// Static assets file.
	r.Static("/images", "./assets/images")
	r.POST("/images", fileHandler.UploadImage)
	// Note: Upload file should in his own route, but somehow it's not always working.

	// Get the address.
	addr := fmt.Sprintf("%s:%s", "", port)
	// Run the gin server.
	r.Run(addr)
}
