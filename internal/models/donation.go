package models

import (
	"encoding/json"
	"errors"
	"filantropi/internal/models/enums"
	"github.com/google/uuid"
	"github.com/midtrans/midtrans-go/snap"
	"gorm.io/gorm"
	"time"
)

// Donation is a struct model for models.User's donation to some models.Fundraising.
type Donation struct {
	ID            string               `json:"id"`
	UserID        int                  `gorm:"not null" json:"user_id"`
	User          UserOutput           `json:"user"`
	FundraisingID int                  `gorm:"not null" json:"fundraising_id"`
	Fundraising   FundraisingOutput    `json:"fundraising"`
	Amount        int                  `gorm:"not null" json:"amount"`
	IsAnonymous   bool                 `gorm:"default:false" json:"is_anonymous"`
	Status        enums.DonationStatus `gorm:"default:'pending'" json:"status"`
	MidtransToken string               `json:"midtrans_token"`
	MidtransURL   string               `json:"midtrans_url"`
	CreatedAt     time.Time            `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt     time.Time            `gorm:"autoUpdateTime" json:"updated_at"`
}

// Note: Gorm will fail if the function signature
//  does not include `*gorm.DB` and `error`

// BeforeCreate is a gorm hook that will be called before creating a donation.
func (donation *Donation) BeforeCreate(tx *gorm.DB) (err error) {
	// UUID version 4
	donation.ID = uuid.NewString()
	return
}

// TableName is a method for overriding the GORM table's name.
func (donation *Donation) TableName() string {
	return "donations"
}

// DonationOutput is a struct model for output the Donation.
type DonationOutput struct {
	ID            string               `json:"id"`
	UserID        int                  `gorm:"not null" json:"user_id"`
	User          UserOutput           `json:"user"`
	FundraisingID int                  `gorm:"not null" json:"fundraising_id"`
	Fundraising   FundraisingOutput    `json:"fundraising"`
	Amount        int                  `gorm:"not null" json:"amount"`
	IsAnonymous   bool                 `gorm:"default:false" json:"is_anonymous"`
	Status        enums.DonationStatus `gorm:"default:'pending'" json:"status"`
}

// TableName is a method for overriding the GORM table's name.
func (_ *DonationOutput) TableName() string {
	return "donations"
}

// DonationCreateInput is a struct for create input value of Donation.
type DonationCreateInput struct {
	Amount      int  `json:"amount" binding:"required"`
	IsAnonymous bool `json:"is_anonymous"`
}

// DonationUpdateInput is a struct for update input value of Donation.
type DonationUpdateInput struct {
	IsAnonymous bool `json:"is_anonymous"`
}

// AddSnapResponse is a method of Donation for adding the snap.Response into models.
func (donation *Donation) AddSnapResponse(response snap.Response) {
	donation.MidtransToken = response.Token
	donation.MidtransURL = response.RedirectURL
}

// AsAnonymous is a method of Donation for make the Donation be anonymous
func (donation *Donation) AsAnonymous() {
	if donation.IsAnonymous {
		donation.UserID = 0
		donation.User = UserOutput{
			ID:    donation.UserID,
			Name:  "Anonymous",
			Photo: "0f663f39cb654120ab9b8b2a7985d264.png",
		}
	}
}

// FromMapInterface is a method of Donation to convert map[string]interface{} into Donation.
func (donation *Donation) FromMapInterface(m map[string]interface{}) error {
	jsonStr, err := json.Marshal(m)
	err = json.Unmarshal(jsonStr, &donation)

	return err
}

// FromCreateInput is a method of Donation to convert DonationCreateInput into Donation.
func (donation *Donation) FromCreateInput(input *DonationCreateInput, userID int,
	fundraisingID int) error {
	jsonStr, err := json.Marshal(input)
	err = json.Unmarshal(jsonStr, &donation)

	// Assign the userID and fundraisingID.
	donation.UserID = userID
	donation.FundraisingID = fundraisingID

	// Return the value and error.
	return err
}

// FromUpdateInput is a method of Donation to convert DonationUpdateInput into Donation.
func (donation *Donation) FromUpdateInput(input *DonationUpdateInput) error {
	jsonStr, err := json.Marshal(input)
	err = json.Unmarshal(jsonStr, &donation)

	return err
}

func (v *DonationCreateInput) Validate() error {
	if v.Amount < 0 {
		return errors.New("the amount of donation cannot less than 0")
	}
	return nil
}
