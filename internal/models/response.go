package models

import (
	"encoding/json"
	"filantropi/configs"
	"fmt"
	"github.com/gin-gonic/gin"
	"math"
	"net/http"
)

// Response is a struct for http response body.
type Response struct {
	ApiVersion string         `json:"api_version"`
	StatusCode int            `json:"status_code"`
	Message    string         `json:"message"`
	Data       interface{}    `json:"data,omitempty"`
	Error      *ResponseError `json:"error,omitempty"`
}

// ResponseError is a struct for describing an error.
type ResponseError struct {
	Location string `json:"location"`
	Detail   string `json:"detail"`
}

// ResponseDataFile is a data inside Response for after uploading a file.
type ResponseDataFile struct {
	Name string `json:"name"`
}

// Init is a function for building a http response body.
func (r *Response) Init() *Response {
	r.ApiVersion = configs.API_VERSION
	return r
}

// Success is a method for building a http success response body.
// message is a custom message for response.
// data is a data that will be sent.
func (r *Response) Success(message string, statusCode int, data interface{}) *Response {
	// Init default response.
	r.Init()
	r.Error = nil

	// Populate with parameter.
	if message != "" {
		r.Message = message
	}
	r.StatusCode = statusCode
	r.Data = data

	return r
}

// Fail is a method for building a http fail response body.
// message is a custom message for response.
// errors is an error message that happen.
func (r *Response) Fail(message string, statusCode int, error *ResponseError) *Response {
	// Init default response.
	r.Init()
	r.Data = nil

	// Populate with parameter.
	if message != "" {
		r.Message = message
	}
	r.StatusCode = statusCode
	r.Error = error

	return r
}

// InternalServerError is a method for building http response body for a failure that caused by server.
func (r *Response) InternalServerError(message string, location string, detail string) *Response {
	errorResponse := &ResponseError{
		Location: location,
		Detail:   detail,
	}
	return r.Fail(message, http.StatusInternalServerError, errorResponse)
}

// NotFound is a method for building http response body for a failure that caused by something not founded.
func (r *Response) NotFound(message string, location string, detail string) *Response {
	errorResponse := &ResponseError{
		Location: location,
		Detail:   detail,
	}
	return r.Fail(message, http.StatusNotFound, errorResponse)
}

// BadRequest is a method for building http response body for a failure that caused input is not valid.
func (r *Response) BadRequest(message string, location string, detail string) *Response {
	errorResponse := &ResponseError{
		Location: location,
		Detail:   detail,
	}
	return r.Fail(message, http.StatusBadRequest, errorResponse)
}

// Unauthorized is a method of Response for Unauthorized response message.
func (r *Response) Unauthorized(message string, detail string, ctx *gin.Context) *Response {
	if message == "" {
		message = "User is not authorized"
	}
	if detail == "" {
		detail = fmt.Sprintf("API %s %s is protected", ctx.Request.Method, ctx.FullPath())
	}

	errorResponse := &ResponseError{
		Location: "path",
		Detail:   detail,
	}
	return r.Fail(message, http.StatusUnauthorized, errorResponse)
}

// Forbidden is a method of Response for Forbidden response message.
func (r *Response) Forbidden(message string, detail string, ctx *gin.Context) *Response {
	if message == "" {
		message = "You don't have access over this path"
	}
	if detail == "" {
		detail = fmt.Sprintf("API %s %s is protected", ctx.Request.Method, ctx.FullPath())
	}

	errorResponse := &ResponseError{
		Location: "path",
		Detail:   detail,
	}
	return r.Fail(message, http.StatusForbidden, errorResponse)
}

// ResponsePagination is a models union of PaginationInput and Response with additional information
type ResponsePagination struct {
	Response
	PaginationInput
	ItemsInPage int `json:"items_in_page"`
	TotalPages  int `json:"total_pages"`
	TotalItems  int `json:"total_items"`
}

// Pagination is a method of ResponsePagination to building necessary information about pagination.
func (r *ResponsePagination) Pagination(input *PaginationInput, itemInPage int, totalItems int) *ResponsePagination {
	r.Search = input.GetSearch()
	r.Limit = input.GetLimit()
	r.Page = input.GetPage()
	r.Sort = input.GetSort()
	r.Order = input.GetOrder()
	r.TotalItems = totalItems
	r.TotalPages = int(math.Ceil(float64(totalItems) / float64(input.GetLimit())))
	r.ItemsInPage = itemInPage

	return r
}

// Parse is a method of ResponsePagination to parse the Response into ResponsePagination
func (r *ResponsePagination) Parse(response *Response) *ResponsePagination {
	jsonStr, _ := json.Marshal(response)
	_ = json.Unmarshal(jsonStr, &r)

	return r
}
