package enums

type Role string

const (
	USER_REGULAR = "user"
	USER_ADMIN   = "admin"
)
