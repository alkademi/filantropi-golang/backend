package enums

type DonationStatus string

const (
	STATUS_SUCCESS   = "success"
	STATUS_PENDING   = "pending"
	STATUS_FAILURE   = "failure"
	STATUS_CHALLENGE = "challenge"
)
