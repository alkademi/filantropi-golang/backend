package models

import (
	"filantropi/configs"
	"strings"
)

// PaginationInput is a model for pagination input query.
type PaginationInput struct {
	Limit  int    `json:"limit" form:"limit"`
	Page   int    `json:"page" form:"page"`
	Sort   string `json:"sort" form:"sort"`
	Order  string `json:"order" form:"order"`
	Search string `json:"search" form:"search"`
}

// GetOffset is a method for get the offset from page and limit.
func (p *PaginationInput) GetOffset() int {
	return (p.GetPage() - 1) * p.GetLimit()
}

// GetLimit is a method for normalize the limit with default value.
func (p *PaginationInput) GetLimit() int {
	if p.Limit <= 0 {
		p.Limit = configs.PAGINATION_LIMIT
	}
	return p.Limit
}

// GetPage is a method for normalize the page with default value.
func (p *PaginationInput) GetPage() int {
	if p.Page <= 0 {
		p.Page = configs.PAGINATION_PAGE
	}
	return p.Page
}

// GetSort is a method for normalize the sort with default value.
func (p *PaginationInput) GetSort() string {
	if p.Sort == "" {
		p.Sort = configs.PAGINATION_SORT
	}
	return p.Sort
}

// GetOrder is a method for normalize the sort with default value.
func (p *PaginationInput) GetOrder() string {
	if p.Order == "" {
		p.Order = configs.PAGINATION_ORDER
	}
	p.Order = strings.ToLower(p.Order)
	return p.Order
}

// GetSearch is a method for normalize the search with default value.
func (p *PaginationInput) GetSearch() string {
	if p.Search == "" {
		p.Search = configs.PAGINATION_SEARCH
	}
	return p.Search
}
