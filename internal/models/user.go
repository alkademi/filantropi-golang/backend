package models

import (
	"encoding/json"
	"filantropi/internal/models/enums"
	"time"
)

// User represent the User in the application.
// Photo is represented as URL to the User Photo.
// Role can be 'user' or 'admin'.
type User struct {
	ID          int
	Name        string        `gorm:"not null" json:"name"`
	Email       string        `gorm:"not null" json:"email"`
	PhoneNumber string        `json:"phone_number"`
	Photo       string        `json:"photo"`
	Username    string        `gorm:"unique;not null" json:"username"`
	IsVerified  bool          `gorm:"default:false" json:"is_verified"`
	Hash        string        `gorm:"not null" json:"-"`
	Role        enums.Role    `gorm:"not null;default:user" json:"role"`
	Fundraising []Fundraising `gorm:"foreignKey:FundraiserID" json:"fundraising,omitempty"`
	Donation    []Donation    `gorm:"foreignKey:UserID" json:"donation,omitempty"`
	CreatedAt   time.Time     `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt   time.Time     `gorm:"autoUpdateTime" json:"updated_at"`
}

// TableName is a method for overriding the GORM table's name.
func (_ *User) TableName() string {
	return "users"
}

// UserOutput represent the unconfident User data in the application.
type UserOutput struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	Email      string `json:"email"`
	Photo      string `json:"photo"`
	IsVerified bool   `json:"is_verified"`
}

// TableName is a method for overriding the GORM table's name.
func (_ *UserOutput) TableName() string {
	return "users"
}

// UserCreateInput is a data structure for input a new user.
type UserCreateInput struct {
	Name        string `json:"name" binding:"required"`
	Email       string `json:"email" binding:"required"`
	PhoneNumber string `json:"phone_number" binding:"required"`
	Photo       string `json:"photo"`
	Username    string `json:"username" binding:"required"`
	Password    string `json:"password" binding:"required"`
}

// UserUpdateInput is a data structure for update existing user.
type UserUpdateInput struct {
	Name        string `json:"name"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
	Photo       string `json:"photo"`
	Password    string `json:"password"`
}

type UserRegisterInput struct {
	Name        string `json:"name" binding:"required"`
	PhoneNumber string `json:"phone_number" binding:"required"`
	Username    string `json:"username" binding:"required"`
	Password    string `json:"password" binding:"required"`
}

// UserLoginInput is a data structure that user enter when login.
type UserLoginInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// ToUser is a method of UserCreateInput to convert it into User.
func (input UserCreateInput) ToUser(hash string) (*User, error) {
	// Init the new user.
	user := &User{}

	// Parse the input to user.
	jsonStr, err := json.Marshal(input)
	err = json.Unmarshal(jsonStr, &user)

	// Add additional information.
	user.Hash = hash
	user.Role = enums.USER_REGULAR

	return user, err
}

// ToUser is a method of UserUpdateInput to convert it into User.
func (input UserUpdateInput) ToUser(hash string) (*User, error) {
	// Init the new user.
	user := &User{}

	// Parse the input to user.
	jsonStr, err := json.Marshal(input)
	err = json.Unmarshal(jsonStr, &user)

	// Add additional information.
	user.Hash = hash

	return user, err
}

// FromMapInterface is a method of User to convert map[string]interface{} into User.
func (f *User) FromMapInterface(m map[string]interface{}) error {
	jsonStr, err := json.Marshal(m)
	err = json.Unmarshal(jsonStr, &f)

	return err
}
