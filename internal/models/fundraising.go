package models

import (
	"encoding/json"
	"errors"
	"time"
)

// Fundraising represent a fundraising that User create.
type Fundraising struct {
	ID              int        `json:"id"`
	FundraiserID    int        `json:"fundraiser_id"`
	Title           string     `gorm:"not null" json:"title"`
	Description     string     `json:"description"`
	Receiver        string     `json:"receiver"`
	TargetDonation  int        `gorm:"not null" json:"target_donation"`
	TargetDate      time.Time  `gorm:"not null" json:"target_date"`
	Donation        []Donation `gorm:"foreignKey:FundraisingID" json:"donation,omitempty"`
	CurrentDonation int        `gorm:"default:0" json:"current_donation"`
	TakenDonation   int        `gorm:"default:0" json:"taken_donation"`
	CountDonor      int        `gorm:"default:0" json:"count_donor"`
	IsActive        bool       `gorm:"default:true" json:"is_active"`
	CreatedAt       time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt       time.Time  `gorm:"autoUpdateTime" json:"updated_at"`
}

// TableName is a method for overriding the GORM table's name.
func (_ *Fundraising) TableName() string {
	return "fundraisings"
}

// FundraisingOutput is a data structure for fundraising in some Donation.
type FundraisingOutput struct {
	ID             int       `json:"id"`
	FundraiserID   int       `json:"fundraiser_id"`
	Title          string    `gorm:"not null" json:"title"`
	Description    string    `json:"description"`
	Receiver       string    `json:"receiver"`
	TargetDonation int       `gorm:"not null" json:"target_donation"`
	TargetDate     time.Time `gorm:"not null" json:"target_date"`
	IsActive       bool      `gorm:"default:true" json:"is_active"`
}

// TableName is a method for overriding the GORM table's name.
func (_ *FundraisingOutput) TableName() string {
	return "fundraisings"
}

// FundraisingOutput is a data structure for fundraising in some Donation.
type FundraisingCloseReport struct {
	ID            int               `json:"id"`
	UserID        int               `gorm:"not null" json:"user_id"`
	User          UserOutput        `json:"user"`
	FundraisingID int               `gorm:"not null" json:"fundraising_id"`
	Fundraising   FundraisingOutput `json:"fundraising"`
	Category      string            `gorm:"not null" json:"category"`
	Description   string            `json:"description"`
	CreatedAt     time.Time         `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt     time.Time         `gorm:"autoUpdateTime" json:"updated_at"`
}

// TableName is a method for overriding the GORM table's name.
func (_ *FundraisingCloseReport) TableName() string {
	return "fundraisings_close_report"
}

// FundraisingCreateInput is a struct for the user input to creating Fundraising
type FundraisingCreateInput struct {
	Title          string    `json:"title" binding:"required"`
	Description    string    `json:"description"`
	Receiver       string    `json:"receiver" binding:"required"`
	TargetDonation int       `json:"target_donation" binding:"required"`
	TargetDate     time.Time `json:"target_date" binding:"required"`
}

// FundraisingUpdateInput is a struct for the user input to updating Fundraising
type FundraisingUpdateInput struct {
	Title          string    `json:"title"`
	Description    string    `json:"description"`
	Receiver       string    `json:"receiver"`
	TargetDonation int       `json:"target_donation"`
	TargetDate     time.Time `json:"target_date"`
}

// FundraisingCloseInput is a struct for the user input to creating Fundraising
type FundraisingCloseInput struct {
	Category    string `json:"category" binding:"required"`
	Description string `json:"description"`
}

// FromMapInterface is a method of Fundraising to convert map[string]interface{} into Fundraising.
func (f *Fundraising) FromMapInterface(m map[string]interface{}) error {
	jsonStr, err := json.Marshal(m)
	err = json.Unmarshal(jsonStr, &f)

	return err
}

// ToFundraising is a method of FundraisingCreateInput to convert it into Fundraising.
func (f FundraisingCreateInput) ToFundraising(fundraiserId int) (*Fundraising, error) {
	fundraising := &Fundraising{}

	jsonStr, err := json.Marshal(f)
	err = json.Unmarshal(jsonStr, &fundraising)

	// Add fundraiserId.
	fundraising.FundraiserID = fundraiserId
	fundraising.IsActive = true

	return fundraising, err
}

// ToFundraising is a method of FundraisingUpdateInput to convert it into Fundraising.
func (f FundraisingUpdateInput) ToFundraising() (*Fundraising, error) {
	fundraising := &Fundraising{}

	jsonStr, err := json.Marshal(f)
	err = json.Unmarshal(jsonStr, &fundraising)

	return fundraising, err
}

// Validate is a method of FundraisingCreateInput to validate the input.
func (f *FundraisingCreateInput) Validate() error {
	if f.TargetDonation <= 0 {
		return errors.New("target donation must greater than 0")
	}
	if f.TargetDate.Before(time.Now()) {
		return errors.New("target date must be after current time")
	}
	return nil
}

// Validate is a method of FundraisingUpdateInput to validate the input.
func (f *FundraisingUpdateInput) Validate() error {
	if f.TargetDonation != 0 {
		if f.TargetDonation < 0 {
			return errors.New("target donation must greater than 0")
		}
	}
	if !f.TargetDate.IsZero() {
		if f.TargetDate.Before(time.Now()) {
			return errors.New("target date must be after current time")
		}
	}
	return nil
}

// FromCloseInput is a method of Donation to convert DonationCloseInput into Donation.
func (v *FundraisingCloseReport) FromCloseInput(input *FundraisingCloseInput, userID int,
	fundraisingID int) error {
	jsonStr, err := json.Marshal(input)
	err = json.Unmarshal(jsonStr, &v)

	// Assign the userID and fundraisingID.
	v.UserID = userID
	v.FundraisingID = fundraisingID

	//Initialize Description if nothing is supplied
	if v.Description == "" {
		v.Description = "none"
	}

	return err
}
