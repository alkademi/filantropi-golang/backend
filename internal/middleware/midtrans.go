package middleware

import (
	"filantropi/configs"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"github.com/gin-gonic/gin"
	"strings"
)

// VerifyMidtransNotification is a middleware for handle webhook notification payment status
// from midtrans. It will verify the signature.
func VerifyMidtransNotification() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Initialize response.
		response := models.Response{}

		// Get the payload from request body.
		payload, err := helpers.ParseRequestBody(c)
		if err != nil {
			response.InternalServerError(
				"[Middleware] Cannot parse the request body",
				"body",
				err.Error())
			c.AbortWithStatusJSON(response.StatusCode, response)
			return
		}

		// Get important payload field.
		orderId := payload["order_id"].(string)
		statusCode := payload["status_code"].(string)
		grossAmount := payload["gross_amount"].(string)
		signatureKey := payload["signature_key"].(string)

		// Verifying notification authenticity using SHA512 encoder.
		// signatureKey from payload field should be verified with this formula.
		// SHA512(order_id + status_code + gross_amount + ServerKey) => Midtrans Docs.
		hashValueString := helpers.GenerateMidtransSignatureKey(orderId, statusCode, grossAmount)

		// Check if signature key is valid
		if strings.Compare(hashValueString, signatureKey) != 0 {
			response.Forbidden(
				"The signature is not valid", "body", c)
			c.AbortWithStatusJSON(response.StatusCode, response)
			return
		}

		// Move the payload into context.
		c.Set(configs.CONTEXT_MIDTRANS_PAYLOAD, payload)
	}
}
