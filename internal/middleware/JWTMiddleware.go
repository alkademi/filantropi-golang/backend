package middleware

import (
	"filantropi/configs"
	"filantropi/internal/helpers"
	"filantropi/internal/models/enums"
	"filantropi/internal/services"
	"github.com/gin-gonic/gin"
)

// JWTMiddleware is a struct for middleware JWT.
type JWTMiddleware struct {
	UserService services.IUserService
}

// InitJWTMiddleware is a function for initialize the UserHandler
func InitJWTMiddleware() JWTMiddleware {
	return JWTMiddleware{
		UserService: &services.UserService{},
	}
}

// Authorize is a middleware for authenticated user by jwt token in his authorization header
// with Bearer Schema.
// It will set configs.CONTEXT_USER and configs.CONTEXT_USER_IS_ADMIN in context data.
func (m *JWTMiddleware) Authorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Authenticate the user.
		username, response, err := helpers.AuthorizationHeaderValidator(c)
		if err != nil {
			c.AbortWithStatusJSON(response.StatusCode, response)
			return
		}

		// Get the user.
		user, _, err := m.UserService.GetBy("username", username)
		if err != nil {
			response.InternalServerError(
				"Authorize middleware error",
				"server",
				err.Error())
			c.AbortWithStatusJSON(response.StatusCode, response)
			return
		}

		// Get the user role.
		isAdmin := false
		if user.Role == enums.USER_ADMIN {
			isAdmin = true
		}

		// Set the context value.
		c.Set(configs.CONTEXT_USER, user)
		c.Set(configs.CONTEXT_USER_IS_ADMIN, isAdmin)
	}
}

// AuthorizeAdmin is a middleware for authenticated user by jwt token in his authorization header
// with Bearer Schema. The user MUST an Admin.
// It will set configs.CONTEXT_USER and configs.CONTEXT_USER_IS_ADMIN in context data.
func (m *JWTMiddleware) AuthorizeAdmin() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Authenticate the user.
		username, response, err := helpers.AuthorizationHeaderValidator(c)
		if err != nil {
			c.AbortWithStatusJSON(response.StatusCode, response)
			return
		}

		// Get user and his role.
		user, _, err := m.UserService.GetBy("username", username)
		if err != nil {
			response.InternalServerError(
				"Authorize middleware error",
				"server",
				err.Error())
			c.AbortWithStatusJSON(response.StatusCode, response)
			return
		}

		// Get the user role.
		isAdmin := false
		if user.Role == enums.USER_ADMIN {
			isAdmin = true
		}

		// Check if user is an admin.
		if !isAdmin {
			response.Forbidden("", "", c)
			c.AbortWithStatusJSON(response.StatusCode, response)
			return
		}

		// Set the context value.
		c.Set(configs.CONTEXT_USER, user)
		c.Set(configs.CONTEXT_USER_IS_ADMIN, isAdmin)
	}
}
