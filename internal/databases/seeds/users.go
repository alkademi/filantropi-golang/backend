package seeds

import (
	database "filantropi/internal/databases"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"time"
)

func GenerateAdmin() {
	// Admin username and password.
	username := "admin"
	password := "admin"

	// Hash the password.
	hash, _ := helpers.HashPassword(password)

	// Create a new admin.
	admin := models.User{
		Name:        "Admin",
		Email:       "admin@admin.com",
		PhoneNumber: "0123456789",
		Photo:       "",
		Username:    username,
		IsVerified:  true,
		Hash:        hash,
		Role:        enums.USER_ADMIN,
		CreatedAt:   time.Time{},
		UpdatedAt:   time.Time{},
	}

	// Insert admin into database.
	database.DB.FirstOrCreate(&admin)
}

func GenerateUser() {
	// User username and password.
	username := "user"
	password := "user"

	// Hash the password.
	hash, _ := helpers.HashPassword(password)

	// Create a new user.
	user := models.User{
		Name:        "User",
		Email:       "user@user.com",
		PhoneNumber: "0123456789",
		Photo:       "",
		Username:    username,
		IsVerified:  true,
		Hash:        hash,
		Role:        enums.USER_REGULAR,
		CreatedAt:   time.Time{},
		UpdatedAt:   time.Time{},
	}

	// Insert user into database.
	database.DB.FirstOrCreate(&user)
}
