package seeds

// Populate is used to populate the database with basic data.
func Populate() {
	// Create a new seed.
	GenerateAdmin()
	GenerateUser()
}
