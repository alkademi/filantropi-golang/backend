package database

import (
	"filantropi/internal/models"
	"fmt"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func Connect() {
	h := os.Getenv("POSTGRES_HOST")
	p := os.Getenv("POSTGRES_PORT")
	d := os.Getenv("POSTGRES_DB")
	u := os.Getenv("POSTGRES_USER")
	pwd := os.Getenv("POSTGRES_PASSWORD")

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta",
		h, u, pwd, d, p)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	db.AutoMigrate(&models.User{})
	db.AutoMigrate(&models.Fundraising{})
	db.AutoMigrate(&models.Donation{})
	db.AutoMigrate(&models.FundraisingCloseReport{})

	DB = db
}
