package mock

import (
	"errors"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"github.com/stretchr/testify/mock"
	"net/http"
)

type DonationServiceMock struct {
	Mock mock.Mock
}

func (service *DonationServiceMock) GetById(id string) (*models.Donation, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(id)

	if arguments.Get(0) == nil {
		response.NotFound("", "param", "Fundraising is not found")
		return nil, response, errors.New("errors")
	}

	donation := arguments.Get(0).(models.Donation)
	response.Success("", http.StatusOK, donation)

	return &donation, response, nil
}

func (service *DonationServiceMock) GetPagination(input *models.PaginationInput) ([]models.Donation, *models.ResponsePagination, error) {
	// Initialize Variables.
	var listDonation []models.Donation
	response := &models.ResponsePagination{}

	// Get the arguments.
	arguments := service.Mock.Called(input)

	// The nil case.
	if arguments.Get(0) == nil {
		response.NotFound("Donation is not found", "param", "")
		return nil, response, errors.New("errors")
	}

	// The non-nil case.
	donation := arguments.Get(0).(models.Donation)
	for i := 0; i < input.GetLimit(); i++ {
		listDonation = append(listDonation, donation)
	}

	// Add pagination detail to response.
	response.Pagination(input, len(listDonation), len(listDonation)*10)

	// Create response message.
	response.Success("Successfully get pagination listDonation", http.StatusOK, listDonation)

	return listDonation, response, nil
}

func (service *DonationServiceMock) Create(fundraisingId int, input *models.DonationCreateInput, user *models.User) (*models.Donation, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}
	donation := &models.Donation{}

	// Get the arguments.
	arguments := service.Mock.Called(fundraisingId)

	if arguments.Get(0) == nil {
		response.NotFound("Fundraising is not found", "param", "")
		return nil, response, errors.New("errors")
	}

	// Init the new fundraising.
	err := donation.FromCreateInput(input, user.ID, fundraisingId)
	if err != nil {
		response.InternalServerError("Error when parse the input", "server", "")
		return nil, response, err
	}

	// Return response message.
	response.Success("", http.StatusCreated, donation)
	return donation, response, nil
}

func (service *DonationServiceMock) UpdateStatus(id string, status enums.DonationStatus) (
	*models.Donation, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(id)

	if arguments.Get(0) == nil {
		response.NotFound("", "param", "Fundraising is not found")
		return nil, response, errors.New("errors")
	}

	donation := arguments.Get(0).(models.Donation)

	// Update the status.
	donation.Status = status

	return &donation, response, nil
}
