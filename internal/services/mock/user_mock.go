package mock

import (
	"errors"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"github.com/stretchr/testify/mock"
	"net/http"
)

type UserServiceMock struct {
	Mock mock.Mock
}

func (service *UserServiceMock) GetBy(field string, value string) (*models.User, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(field, value)

	if arguments.Get(0) == nil {
		response.NotFound("", "param", "User is not found")
		return nil, response, errors.New("errors")
	}

	user := arguments.Get(0).(models.User)
	response.Success("", http.StatusOK, user)

	return &user, response, nil
}

func (service *UserServiceMock) IsAdmin(id string, username string) (bool, *models.Response, error) {
	//TODO implement me
	panic("implement me")
}

func (service *UserServiceMock) Create(input *models.UserCreateInput) (*models.User, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(input)

	if arguments.Get(0) == nil {
		response.NotFound("User is not found", "param", "")
		return nil, response, errors.New("errors")
	}

	// Check if the username is already taken.
	userExist, _, _ := service.GetBy("username", input.Username)
	if userExist != nil {
		response.BadRequest("Username is already taken", "param", "")
		return nil, response, errors.New("errors")
	}

	// Hash the password.
	hash, _ := helpers.HashPassword(input.Password)
	// Change the user input to match the response data.
	user, _ := input.ToUser(hash)

	// Return response message.
	response.Success("", http.StatusCreated, user)
	return user, response, nil
}

func (service *UserServiceMock) Update(id int, input *models.UserUpdateInput) (*models.User, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(input)

	if arguments.Get(0) == nil {
		response.NotFound("Fundraising is not found", "param", "")
		return nil, response, errors.New("errors")
	}

	// Hash the password.
	hash, _ := helpers.HashPassword(input.Password)
	// Change the user input to match the response data.
	user, _ := input.ToUser(hash)

	// Return response message.
	response.Success("", http.StatusOK, user)
	return user, response, nil
}

func (service *UserServiceMock) Delete(id string) (*models.User, *models.Response, error) {
	//TODO implement me
	panic("implement me")
}
