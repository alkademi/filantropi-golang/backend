package mock

import (
	"errors"
	"filantropi/internal/models"
	"github.com/stretchr/testify/mock"
	"net/http"
)

type FundraisingServiceMock struct {
	Mock mock.Mock
}

func (service *FundraisingServiceMock) GetById(id string) (*models.Fundraising, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(id)

	if arguments.Get(0) == nil {
		response.NotFound("", "param", "Fundraising is not found")
		return nil, response, errors.New("errors")
	}

	fundraising := arguments.Get(0).(models.Fundraising)
	response.Success("", http.StatusOK, fundraising)

	return &fundraising, response, nil
}

func (service *FundraisingServiceMock) GetPaginated(input *models.PaginationInput) ([]models.Fundraising, *models.ResponsePagination, error) {
	// Initialize Variables.
	var listFundraising []models.Fundraising
	response := &models.ResponsePagination{}

	// Get the arguments.
	arguments := service.Mock.Called(input)

	// The nil case.
	if arguments.Get(0) == nil {
		response.NotFound("Fundraising is not found", "param", "")
		return nil, response, errors.New("errors")
	}

	// The non-nil case.
	fundraising := arguments.Get(0).(models.Fundraising)
	for i := 0; i < input.GetLimit(); i++ {
		listFundraising = append(listFundraising, fundraising)
	}

	// Add pagination detail to response.
	response.Pagination(input, len(listFundraising), len(listFundraising)*10)

	// Create response message.
	response.Success("Successfully get pagination listFundraising", http.StatusOK, listFundraising)

	return listFundraising, response, nil
}

func (service *FundraisingServiceMock) Create(fundraiserId int, input *models.FundraisingCreateInput) (*models.Fundraising, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(fundraiserId)

	if arguments.Get(0) == nil {
		response.NotFound("Fundraiser is not found", "param", "")
		return nil, response, errors.New("errors")
	}

	// Init the new fundraising.
	fundraising, err := input.ToFundraising(fundraiserId)
	if err != nil {
		response.InternalServerError("Error when parse the input", "server", "")
		return nil, response, err
	}

	// Return response message.
	response.Success("", http.StatusCreated, fundraising)
	return fundraising, response, nil
}

func (service *FundraisingServiceMock) Update(id string, input *models.FundraisingUpdateInput) (*models.Fundraising, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(id)

	if arguments.Get(0) == nil {
		response.NotFound("Fundraising is not found", "param", "")
		return nil, response, errors.New("errors")
	}

	// Get fundraising from mock.
	fundraising := arguments.Get(0).(models.Fundraising)

	// Update the fundraising.
	if input.Title != "" {
		fundraising.Title = input.Title
	}
	if input.Description != "" {
		fundraising.Description = input.Description
	}
	if input.Receiver != "" {
		fundraising.Receiver = input.Receiver
	}
	if input.TargetDonation != 0 {
		fundraising.TargetDonation = input.TargetDonation
	}
	if !input.TargetDate.IsZero() {
		fundraising.TargetDate = input.TargetDate
	}

	response.Success("", http.StatusOK, fundraising)

	// Return response message.
	return &fundraising, response, nil
}

func (service *FundraisingServiceMock) UpdateDonation(id string, amount int) (*models.Fundraising, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}

	// Get the arguments.
	arguments := service.Mock.Called(id)

	if arguments.Get(0) == nil {
		response.NotFound("", "param", "Fundraising is not found")
		return nil, response, errors.New("errors")
	}

	fundraising := arguments.Get(0).(models.Fundraising)
	fundraising.CurrentDonation += amount
	fundraising.CountDonor++

	response.Success("", http.StatusOK, fundraising)

	return &fundraising, response, nil
}

func (service *FundraisingServiceMock) Close(id string, input *models.FundraisingCloseInput, userID int, fundraisingID int) (*models.Fundraising, *models.Response, error) {
	// Initialize variables.
	response := &models.Response{}
	closeReport := &models.FundraisingCloseReport{}

	// Get the arguments.
	arguments := service.Mock.Called(id)

	if arguments.Get(0) == nil {
		response.NotFound("Fundraising is not found", "param", "")
		return nil, response, errors.New("errors")
	}

	// Get fundraising from mock.
	fundraising := arguments.Get(0).(models.Fundraising)

	// Check if the selected fundraising has already closed
	if !fundraising.IsActive {
		response.BadRequest("", "param", "Fundraising has already closed")
		return nil, response, errors.New("errors")
	}

	// Init the new fundraising closing report
	err := closeReport.FromCloseInput(input, userID, fundraisingID)
	if err != nil {
		response.InternalServerError(
			"",
			"server",
			"Error when parse the input")
		return nil, response, err
	}

	// Update the fundraising.
	fundraising.IsActive = false

	// Return response message.
	response.Success("", http.StatusOK, closeReport)

	// Return response message.
	return &fundraising, response, nil
}
