package services

import (
	database "filantropi/internal/databases"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"gorm.io/gorm/clause"
	"net/http"
)

type IDonationService interface {
	GetById(id string) (*models.Donation, *models.Response, error)
	GetPagination(input *models.PaginationInput) (
		[]models.Donation, *models.ResponsePagination, error)
	Create(fundraisingId int, input *models.DonationCreateInput, user *models.User) (
		*models.Donation, *models.Response, error)
	UpdateStatus(id string, status enums.DonationStatus) (*models.Donation, *models.Response, error)
}

// DonationService is a struct for naming Donation service.
type DonationService struct {
}

// GetById is a DonationService method for fetching a donation by its ID.
func (*DonationService) GetById(id string) (*models.Donation, *models.Response, error) {
	// Initialize variables.
	donation := &models.Donation{}
	response := &models.Response{}

	// Default message.
	response.Message = "Cannot get the Donation by its Id"

	// Find the fundraising in database.
	err := database.DB.Where("id = ?", id).Preload(clause.Associations).First(&donation).Error
	if err != nil {
		response.NotFound(
			"Donation is not found",
			"param",
			err.Error())
		return nil, response, err
	}

	// Make as anonymous.
	donation.AsAnonymous()

	// Return response message.
	response.Success("Successfully get the Donation by its Id", http.StatusOK, donation)

	return donation, response, nil
}

// GetPagination is a DonationService method for fetching a donation by with pagination.
func (*DonationService) GetPagination(input *models.PaginationInput) (
	[]models.Donation, *models.ResponsePagination, error) {
	// Initialize variables.
	var donations []models.Donation
	response := &models.ResponsePagination{}

	// Call the helper function to get pagination.

	//result, response, err := helpers.Pagination(input, models.Donation{})
	queryBuilder, totalItems, err := helpers.PaginationQuery(input, models.Donation{})
	if err != nil {
		response.InternalServerError(
			"Cannot process get Donation with pagination",
			"server", err.Error(),
		)
		return nil, response, err
	}

	// Query the database.
	err = queryBuilder.Preload(clause.Associations).Find(&donations).Error
	if err != nil {
		response.InternalServerError(
			"Cannot process get Donation with pagination",
			"server", err.Error(),
		)
		return nil, response, err
	}

	// Make as anonymous.
	for i := range donations {
		donations[i].AsAnonymous()
	}

	// Add pagination detail to response.
	response.Pagination(input, len(donations), int(totalItems))

	// Return response message.
	response.Success("Successfully get the Donation with Pagination", http.StatusOK, donations)

	return donations, response, nil
}

// Create is a DonationService method for create a new models.Donation from models.DonationCreateInput
func (*DonationService) Create(fundraisingId int, input *models.DonationCreateInput, user *models.User) (*models.Donation, *models.Response, error) {
	midtransHelper := helpers.MidtransHelper{}
	donation := &models.Donation{}
	response := &models.Response{}

	// Initialize midtrans snap service.
	midtransHelper.InitializeSnapClient()

	// Default message.
	response.Message = "Cannot create a new Donation"

	// Init the new fundraising.
	err := donation.FromCreateInput(input, user.ID, fundraisingId)
	if err != nil {
		response.InternalServerError(
			"",
			"server",
			"Error when parse the input")
		return nil, response, err
	}

	// Set the donation status as pending.
	donation.Status = enums.STATUS_PENDING

	// Insert the new user into database.
	err = database.DB.Omit(clause.Associations).Create(&donation).Error
	if err != nil {
		response.InternalServerError(
			"Cannot create donation",
			"database",
			err.Error())
		return nil, response, err
	}
	// Query the inputted donation.
	//database.DB.Preload(clause.Associations).Find(&donation)

	// Create midtrans snap request.
	snapRequest := midtransHelper.SnapRequestDonation(*user, *donation)

	// Create a new midtrans transaction.
	snapResponse, snapError := midtransHelper.Snap.CreateTransaction(snapRequest)
	if snapError != nil {
		response.InternalServerError(
			"Cannot create donation",
			"database",
			snapError.GetMessage())

		// Update the database to change the donation status.
		donation.Status = enums.STATUS_FAILURE
		database.DB.Omit(clause.Associations).Updates(&donation)

		return nil, response, err
	}

	// Add the snapResponse into donation.
	donation.AddSnapResponse(*snapResponse)

	// Update the database to add the snapResponse.
	database.DB.Omit(clause.Associations).Updates(&donation)

	// Return response message.
	response.Success("Successfully created a new Donation", http.StatusCreated, donation)

	return donation, response, nil
}

// UpdateStatus is a DonationService method for update the donation's status.
func (s *DonationService) UpdateStatus(id string, status enums.DonationStatus) (*models.Donation, *models.Response, error) {
	// Initialize variables.
	donation := models.Donation{}
	response := models.Response{}

	err := database.DB.Model(&donation).Omit(clause.Associations).Where("id = ?", id).
		Update("status", status).Error
	if err != nil {
		response.Fail(
			"Cannot update donation's status",
			500,
			nil)
	}

	return &donation, &response, nil
}
