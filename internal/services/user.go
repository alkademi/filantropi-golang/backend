package services

import (
	"errors"
	database "filantropi/internal/databases"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"fmt"
	"net/http"
	"net/mail"
	"strings"
)

type IUserService interface {
	GetBy(field string, value string) (*models.User, *models.Response, error)
	IsAdmin(id string, username string) (bool, *models.Response, error)
	Create(input *models.UserCreateInput) (*models.User, *models.Response, error)
	Update(id int, input *models.UserUpdateInput) (*models.User, *models.Response, error)
	Delete(id string) (*models.User, *models.Response, error)
}

// UserService is a struct for naming user service.
type UserService struct {
}

// GetBy is a service function for getting a user by its field.
func (*UserService) GetBy(field string, value string) (*models.User, *models.Response, error) {
	user := &models.User{}
	response := &models.Response{}
	err := errors.New("userService: GetById Error")

	// Default message.
	response.Message = fmt.Sprintf("Fail to get the User by its %s", field)

	// Check if attribute exist.
	if !database.DB.Migrator().HasColumn(&user, field) {
		response.BadRequest(
			"Fail to get the User",
			"parameter",
			fmt.Sprintf("User model is not have '%s' field", field))
		return nil, response, errors.New(response.Error.Detail)
	}

	// Find the user in database.
	query := fmt.Sprintf("%s = ?", field)
	err = database.DB.Where(query, value).First(&user).Error

	if err != nil {
		response.Error = &models.ResponseError{
			Location: "param",
			Detail:   "User is not found",
		}
		response.StatusCode = http.StatusNotFound
		return nil, response, err
	}

	// Return response message.
	response.Message = "Successfully fetching a user by its id or username"
	response.Data = user
	response.StatusCode = http.StatusOK

	return user, response, nil
}

// IsAdmin is a service function for getting a user by its id or username.
// Fill the parameter with blank string if you won't use that.
func (*UserService) IsAdmin(id string, username string) (bool, *models.Response, error) {
	user := &models.User{}
	response := &models.Response{}
	err := errors.New("userService: IsAdmin Error")

	// Default message.
	response.Message = "Failed to check if user is an Admin"

	// Find the user in database.
	if id != "" {
		err = database.DB.Where("id = ?", id).First(&user).Error
	} else if username != "" {
		err = database.DB.Where("username = ?", username).First(&user).Error
	}

	if err != nil {
		response.NotFound(
			"",
			"param",
			"User is not found")
		return false, response, err
	}

	// Check if user role is Admin.
	isAdmin := false
	if user.Role == enums.USER_ADMIN {
		isAdmin = true
	}

	// Return response message.
	response.Success("Successfully check if user is an Admin", http.StatusOK, user)

	return isAdmin, response, nil
}

// Create is a service function for creating a new user.
func (*UserService) Create(input *models.UserCreateInput) (*models.User, *models.Response, error) {
	user := &models.User{}
	response := &models.Response{}
	err := errors.New("userService: Init Error")

	// Default message.
	response.Message = "Failed to create a new user"

	// Check that username is not already used.
	result := database.DB.Where("username", input.Username).Find(&user)
	if result.RowsAffected > 0 {
		response.Error = &models.ResponseError{
			Location: "body",
			Detail:   "Username is already taken",
		}
		response.StatusCode = http.StatusBadRequest
		return nil, response, err
	}

	// Check email valid
	if input.Email != "" {
		addr, err := mail.ParseAddress(input.Email)
		if err != nil {
			response.Error = &models.ResponseError{
				Location: "body",
				Detail:   "Email is not valid",
			}
			response.StatusCode = http.StatusBadRequest
			return nil, response, err
		}
		if len(strings.Split(addr.Address, "@")) != 2 && len(strings.Split(addr.Address, ".")) != 2 {
			response.Error = &models.ResponseError{
				Location: "body",
				Detail:   "Email is not valid",
			}
			response.StatusCode = http.StatusBadRequest
			return nil, response, err
		}
	}

	// Hash the password.
	hash, err := helpers.HashPassword(input.Password)

	// Convert input to user.
	user, err = input.ToUser(hash)
	if err != nil {
		response.InternalServerError(
			"",
			"server",
			"Error when parse the input")
		return nil, response, err
	}

	// Insert the new user into database.
	database.DB.Create(&user)

	// Return response message.
	response.Message = "Successfully created a new user"
	response.Data = user
	response.StatusCode = http.StatusCreated

	return user, response, nil
}

// Update is a service function for updating a user.
func (*UserService) Update(id int, input *models.UserUpdateInput) (*models.User, *models.Response, error) {
	user := &models.User{}
	response := &models.Response{}

	// Default message.
	response.Message = "Failed to update the user"

	// Search the user in database.
	err := database.DB.Where("id = ?", id).First(&user).Error
	if err != nil {
		response.Error = &models.ResponseError{
			Location: "param",
			Detail:   "User not found",
		}
		response.StatusCode = http.StatusNotFound
		return nil, response, err
	}

	// Hash the password.
	var hash string
	if input.Password != "" {
		hash, err = helpers.HashPassword(input.Password)
	}

	// Convert input to user.
	updatedData, err := input.ToUser(hash)
	if err != nil {
		response.InternalServerError(
			"",
			"server",
			"Error when parse the input")
		return nil, response, err
	}

	// UpdateMine the attribute.
	database.DB.Model(&user).Updates(updatedData)

	// Return response message.
	response.Message = "Successfully update the user"
	response.Data = user
	response.StatusCode = http.StatusOK

	return user, response, nil
}

// Delete is a service function for deleting a user by its id.
func (*UserService) Delete(id string) (*models.User, *models.Response, error) {
	user := &models.User{}
	response := &models.Response{}

	// Default message.
	response.Message = "Failed to delete the user"

	// Search the user in database.
	err := database.DB.Where("id = ?", id).First(&user).Error
	if err != nil {
		response.Error = &models.ResponseError{
			Location: "param",
			Detail:   "User not found",
		}
		response.StatusCode = http.StatusNotFound
		return nil, response, err
	}

	// UpdateMine the attribute.
	database.DB.Delete(&user)

	// Return response message.
	response.Message = "Successfully delete the user"
	response.Data = user
	response.StatusCode = http.StatusOK

	return user, response, nil
}
