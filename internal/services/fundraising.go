package services

import (
	database "filantropi/internal/databases"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"net/http"

	"gorm.io/gorm/clause"
)

type IFundraisingService interface {
	GetById(id string) (
		*models.Fundraising, *models.Response, error)
	GetPaginated(input *models.PaginationInput) (
		[]models.Fundraising, *models.ResponsePagination, error)
	Create(fundraiserId int, input *models.FundraisingCreateInput) (
		*models.Fundraising, *models.Response, error)
	Update(id string, input *models.FundraisingUpdateInput) (
		*models.Fundraising, *models.Response, error)
	UpdateDonation(id string, amount int) (
		*models.Fundraising, *models.Response, error)
	Close(id string, input *models.FundraisingCloseInput, userID int, fundraisingID int) (
		*models.Fundraising, *models.Response, error)
}

// FundraisingService is a struct for naming Fundraising service.
type FundraisingService struct {
}

// GetById is a service method for fetching a fundraising by its id.
func (*FundraisingService) GetById(id string) (*models.Fundraising, *models.Response, error) {
	// Initialize variables.
	fundraising := &models.Fundraising{}
	response := &models.Response{}

	// Default message.
	response.Message = "Failed to fetch a fundraising"

	// Find the fundraising in database.
	err := database.DB.Where("id = ?", id).First(&fundraising).Error
	if err != nil {
		response.NotFound(
			"",
			"param",
			"Fundraising is not found")
		return nil, response, err
	}

	// Return response message.
	response.Success("Successfully fetching a fundraising by its id", http.StatusOK, fundraising)

	return fundraising, response, nil
}

// GetPaginated is a service method for getting Fundraising with pagination.
func (*FundraisingService) GetPaginated(input *models.PaginationInput) (
	[]models.Fundraising, *models.ResponsePagination, error) {
	// Initialize Variables.
	var fundraising []models.Fundraising
	var err error
	response := &models.ResponsePagination{}

	// Call the helper function to get pagination.
	result, response, err := helpers.Pagination(input, models.Fundraising{})

	// Parse result to fundraising.
	fundraising = make([]models.Fundraising, response.ItemsInPage)
	for i, val := range result {
		// Caution: This may cause some error if json tag name not following gorm convention for table name.
		err = fundraising[i].FromMapInterface(val)
	}

	// Check if error happens.
	if err != nil {
		response.InternalServerError(
			"Failed to get paginated fundraisings",
			"server",
			err.Error())
		return nil, response, err
	}

	// Create response message.
	response.Success("Successfully get pagination fundraisings", http.StatusOK, fundraising)

	return fundraising, response, nil
}

// Create is a service method for creating a new fundraising.
func (*FundraisingService) Create(fundraiserId int, input *models.FundraisingCreateInput) (*models.Fundraising, *models.Response, error) {
	fundraising := &models.Fundraising{}
	response := &models.Response{}

	// Default message.
	response.Message = "Failed to create a new fundraising"

	// Init the new fundraising.
	fundraising, err := input.ToFundraising(fundraiserId)
	if err != nil {
		response.InternalServerError(
			"",
			"server",
			"Error when parse the input")
		return nil, response, err
	}

	// Insert the new user into database.
	err = database.DB.Create(&fundraising).Error
	if err != nil {
		response.InternalServerError(
			"",
			"database",
			"Failure happen in server")
		return nil, response, err
	}

	// Return response message.
	response.Success("Successfully created a new fundraising", http.StatusCreated, fundraising)

	return fundraising, response, nil
}

// Update is a service method for updating a fundraising.
func (f *FundraisingService) Update(id string, input *models.FundraisingUpdateInput) (
	*models.Fundraising, *models.Response, error) {
	fundraising := &models.Fundraising{}
	response := &models.Response{}

	// Default message.
	response.Message = "Failed to update a fundraising"

	// Search the fundraising from database by its id.
	err := database.DB.Where("id = ?", id).First(&fundraising).Error
	if err != nil {
		response.NotFound(
			"",
			"param",
			"Fundraising not found")
		return nil, response, err
	}

	// Init the updated fundraising.
	updatedData, err := input.ToFundraising()
	if err != nil {
		response.InternalServerError(
			"",
			"server",
			"Error when parse the input")
		return nil, response, err
	}

	// UpdateMine the fundraising attribute.
	err = database.DB.Model(fundraising).Updates(updatedData).Error

	// Insert the updated data into database.
	if err != nil {
		response.InternalServerError(
			"",
			"database",
			"Failure happen in server")
		return nil, response, err
	}

	// Return response message.
	response.Success("Successfully update a fundraising", http.StatusOK, fundraising)

	return fundraising, response, nil
}

// UpdateDonation is a method of FundraisingService for increment the number of current donation.
func (f *FundraisingService) UpdateDonation(id string, amount int) (
	*models.Fundraising, *models.Response, error) {
	fundraising := &models.Fundraising{}
	response := &models.Response{}

	// Default message.
	response.Message = "Failed to update the donation in the fundraising"

	// Search the fundraising from database by its id.
	err := database.DB.Where("id = ?", id).First(&fundraising).Error
	if err != nil {
		response.NotFound(
			"",
			"param",
			"Fundraising not found")
		return nil, response, err
	}

	// Update the current donation in fundraising.
	fundraising.CountDonor += 1
	fundraising.CurrentDonation += amount

	// Put the update into database.
	err = database.DB.Model(fundraising).Updates(fundraising).Error

	// Insert the updated data into database.
	if err != nil {
		response.InternalServerError(
			"",
			"database",
			"Failure happen in server")
		return nil, response, err
	}

	// Return response message.
	response.Success("Successfully update a fundraising", http.StatusOK, fundraising)

	return fundraising, response, nil
}

// Close is a service method for updating active status of a fundraising.
func (f *FundraisingService) Close(id string, input *models.FundraisingCloseInput, userID int, fundraisingID int) (
	*models.Fundraising, *models.Response, error) {
	fundraising := &models.Fundraising{}
	response := &models.Response{}
	closeReport := &models.FundraisingCloseReport{}

	// Default message.
	response.Message = "Failed to close a fundraising"

	// Search the fundraising from database by its id.
	err := database.DB.Where("id = ?", id).First(&fundraising).Error
	if err != nil {
		response.NotFound(
			"",
			"param",
			"Fundraising not found")
		return nil, response, err
	}

	// Check if the selected fundraising has already closed
	if !fundraising.IsActive {
		response.BadRequest("", "param", "Fundraising has already closed")
		return nil, response, err
	}

	// Init the new fundraising closing report
	err = closeReport.FromCloseInput(input, userID, fundraisingID)
	if err != nil {
		response.InternalServerError(
			"",
			"server",
			"Error when parse the input")
		return nil, response, err
	}

	// Update the fundraising is_active attribute.
	err = database.DB.Model(fundraising).Update("is_active", "false").Error

	// Insert the updated data into database.
	if err != nil {
		response.InternalServerError(
			"",
			"database",
			"Failure happen in server")
		return nil, response, err
	}

	// Insert the new user into database.
	err = database.DB.Omit(clause.Associations).Create(&closeReport).Error
	if err != nil {
		response.InternalServerError(
			"Cannot create a new close report",
			"database",
			err.Error())
		return nil, response, err
	}
	database.DB.Preload(clause.Associations).First(&closeReport)

	// Return response message.
	response.Success("Successfully close a fundraising", http.StatusOK, closeReport)

	return fundraising, response, nil
}
