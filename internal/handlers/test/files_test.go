package test

import (
	"encoding/json"
	"filantropi/configs"
	"filantropi/internal/models"
	"github.com/stretchr/testify/assert"
	"image"
	"image/png"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"testing"
)

func createImage() *image.RGBA {
	width := 200
	height := 100

	upLeft := image.Point{}
	lowRight := image.Point{X: width, Y: height}

	img := image.NewRGBA(image.Rectangle{Min: upLeft, Max: lowRight})

	return img
}

func TestFileHandler_UploadImage(t *testing.T) {
	// Create a new image and write it to the writer.
	img := createImage()
	filename := "test_img.png"

	t.Run("Success", func(t *testing.T) {
		// Set up a pipe to avoid buffering
		pr, pw := io.Pipe()
		// This writers are going to transform what we pass to it to multipart form data
		// and write it to our io.Pipe
		writer := multipart.NewWriter(pw)

		go func() {
			defer writer.Close()
			// We create the form data field which returns another writer to write the actual file

			part, err := writer.CreateFormFile(configs.FILES_IMAGE_FORM_NAME, filename)
			if err != nil {
				t.Error(err)
			}

			// Encode() takes an io.Writer. We pass the multipart field that we defined
			// earlier which, in turn, writes to our io.Pipe
			err = png.Encode(part, img)
			if err != nil {
				t.Error(err)
			}
		}()

		//We read from the pipe which receives data
		//from the multipart writer, which, in turn,
		//receives data from png.Encode().
		//We have 3 chained writers !
		request := httptest.NewRequest(http.MethodPost, "/files/images", pr)
		request.Header.Add("Content-Type", writer.FormDataContentType())

		// Send the request.
		recorder, response, err := SendRequest(t, request)

		// Parse the data.
		data := models.ResponseDataFile{}
		jsonStr, err := json.Marshal(response.Data.(map[string]interface{}))
		err = json.Unmarshal(jsonStr, &data)
		assert.NoError(t, err)

		// Validate the response.
		assert.Equal(t, 201, recorder.Code)
		assert.NotEqual(t, data.Name, filename)

	})

	t.Run("Failure", func(t *testing.T) {
		// Set up a pipe to avoid buffering
		pr, pw := io.Pipe()
		// This writers are going to transform what we pass to it to multipart form data
		// and write it to our io.Pipe
		writer := multipart.NewWriter(pw)

		go func() {
			defer writer.Close()
			// We create the form data field which returns another writer to write the actual file
			part, err := writer.CreateFormFile("upload_file", filename)
			if err != nil {
				t.Error(err)
			}

			// Encode() takes an io.Writer. We pass the multipart field that we defined
			// earlier which, in turn, writes to our io.Pipe
			err = png.Encode(part, img)
			if err != nil {
				t.Error(err)
			}
		}()

		//We read from the pipe which receives data
		//from the multipart writer, which, in turn,
		//receives data from png.Encode().
		//We have 3 chained writers !
		request := httptest.NewRequest(http.MethodPost, "/files/images", pr)
		request.Header.Add("Content-Type", writer.FormDataContentType())

		// A response recorder for getting written http response.
		recorder := httptest.NewRecorder()
		// Send the request.
		r.ServeHTTP(recorder, request)

		// Validate the response.
		assert.Equal(t, 400, recorder.Code)
	})
}
