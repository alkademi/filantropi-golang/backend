package test

import (
	"bytes"
	"encoding/json"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func TestMidtransHandler_DonationNotification(t *testing.T) {
	// Generate the signature key.
	orderId := "FN-52"
	statusCode := "200"
	grossAmount := "4200.00"
	signatureKey := helpers.GenerateMidtransSignatureKey(orderId, statusCode, grossAmount)

	// Test case.
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Input      map[string]interface{}
	}{
		{
			Name:       "Success",
			IsSuccess:  true,
			StatusCode: http.StatusOK,
			Input: map[string]interface{}{
				"transaction_time":   "2022-03-28 16:27:41",
				"transaction_status": "settlement",
				"transaction_id":     "bfeb3009-0ce9-44e5-9447-879ee96322ed",
				"status_message":     "midtrans payment notification",
				"status_code":        statusCode,
				"signature_key":      signatureKey,
				"settlement_time":    "2022-03-28 16:28:05",
				"payment_type":       "bca_klikpay",
				"order_id":           orderId,
				"merchant_id":        "G509435825",
				"gross_amount":       grossAmount,
				"fraud_status":       "accept",
				"currency":           "IDR",
				"approval_code":      "112233",
			},
		},
		{
			Name:       "Fail - Missing Signature Key",
			IsSuccess:  false,
			StatusCode: http.StatusForbidden,
			Input: map[string]interface{}{
				"transaction_time":   "2022-03-28 16:27:41",
				"transaction_status": "settlement",
				"transaction_id":     "bfeb3009-0ce9-44e5-9447-879ee96322ed",
				"status_message":     "midtrans payment notification",
				"status_code":        statusCode,
				"signature_key":      "",
				"settlement_time":    "2022-03-28 16:28:05",
				"payment_type":       "bca_klikpay",
				"order_id":           orderId,
				"merchant_id":        "G509435825",
				"gross_amount":       grossAmount,
				"fraud_status":       "accept",
				"currency":           "IDR",
				"approval_code":      "112233",
			},
		},
		{
			Name:       "Fail - Invalid Signature Key",
			IsSuccess:  false,
			StatusCode: http.StatusForbidden,
			Input: map[string]interface{}{
				"transaction_time":   "2022-03-28 16:27:41",
				"transaction_status": "settlement",
				"transaction_id":     "bfeb3009-0ce9-44e5-9447-879ee96322ed",
				"status_message":     "midtrans payment notification",
				"status_code":        statusCode,
				"signature_key":      signatureKey + "1",
				"settlement_time":    "2022-03-28 16:28:05",
				"payment_type":       "bca_klikpay",
				"order_id":           orderId,
				"merchant_id":        "G509435825",
				"gross_amount":       grossAmount,
				"fraud_status":       "accept",
				"currency":           "IDR",
				"approval_code":      "112233",
			},
		},
	}

	// The valid donation.
	donation := models.Donation{
		ID:            "52",
		UserID:        1,
		User:          models.UserOutput{},
		FundraisingID: 12,
		Fundraising:   models.FundraisingOutput{},
		Amount:        4200,
		IsAnonymous:   false,
		Status:        enums.STATUS_PENDING,
		MidtransToken: "",
		MidtransURL:   "",
		CreatedAt:     time.Time{},
		UpdatedAt:     time.Time{},
	}
	fundraisingId := strconv.Itoa(donation.FundraisingID)

	// The valid fundraising.
	fundraising := models.Fundraising{
		ID:              donation.FundraisingID,
		FundraiserID:    8,
		Title:           "Test",
		Description:     "Test",
		Receiver:        "Test",
		TargetDonation:  40000,
		TargetDate:      time.Time{},
		Donation:        nil,
		CurrentDonation: 0,
		TakenDonation:   0,
		CountDonor:      0,
		IsActive:        true,
		CreatedAt:       time.Time{},
		UpdatedAt:       time.Time{},
	}

	// Program the mock.
	donationService.Mock.On("GetById", donation.ID).Return(donation)
	donationService.Mock.On("GetById", "0").Return(nil)

	donationService.Mock.On("UpdateStatus", donation.ID).Return(donation)
	donationService.Mock.On("UpdateStatus", "0").Return(nil)

	fundraisingService.Mock.On("GetById", fundraisingId).Return(fundraising)
	fundraisingService.Mock.On("GetById", "0").Return(nil)

	fundraisingService.Mock.On("UpdateDonation", fundraisingId).Return(fundraising)
	fundraisingService.Mock.On("UpdateDonation", "0").Return(nil)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			reqBody, _ := json.Marshal(test.Input)
			url := fmt.Sprintf("/webhooks/midtrans/")
			request, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(reqBody))

			// Send the request.
			recorder, response, _ := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.Nil(t, response.Error)
		})
	}
}
