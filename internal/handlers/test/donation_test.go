package test

import (
	"bytes"
	"encoding/json"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func TestDonationHandler_GetById(t *testing.T) {
	// Test case.
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		DonationId string
	}{
		{
			Name:       "Success",
			IsSuccess:  true,
			StatusCode: http.StatusOK,
			DonationId: "1",
		},
		{
			Name:       "Not Found",
			IsSuccess:  false,
			StatusCode: http.StatusNotFound,
			DonationId: "2",
		},
	}

	// The valid donation.
	donation := models.Donation{
		ID:            "1",
		UserID:        42,
		User:          models.UserOutput{},
		FundraisingID: 1,
		Fundraising:   models.FundraisingOutput{},
		Amount:        4200,
		IsAnonymous:   false,
		Status:        "Success",
		MidtransToken: "",
		MidtransURL:   "",
		CreatedAt:     time.Time{},
		UpdatedAt:     time.Time{},
	}

	// Program the mock.
	donationService.Mock.On("GetById", "1").Return(donation)
	donationService.Mock.On("GetById", "2").Return(nil)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			request, err := http.NewRequest(http.MethodGet,
				fmt.Sprintf("/donations/%s", test.DonationId), nil)

			// Send the request.
			recorder, response, err := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			respDonation := models.Donation{}
			err = respDonation.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, donation, respDonation)
		})
	}
}

func TestDonationHandler_GetByFundraisingId(t *testing.T) {
	tests := []struct {
		Name          string
		IsSuccess     bool
		StatusCode    int
		Token         string
		FundraisingID int
		Input         *models.PaginationInput
	}{
		{
			Name:          "Success",
			IsSuccess:     true,
			FundraisingID: 1,
			Input: &models.PaginationInput{
				Limit: 10,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:          "Default Limit",
			IsSuccess:     true,
			FundraisingID: 1,
			Input: &models.PaginationInput{
				Limit: -1,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
	}

	// The valid donation.
	donation := models.Donation{
		ID:            "1",
		UserID:        userData.User.ID,
		User:          models.UserOutput{},
		FundraisingID: 1,
		Fundraising:   models.FundraisingOutput{},
		Amount:        4200,
		IsAnonymous:   false,
		Status:        enums.STATUS_SUCCESS,
		MidtransToken: "",
		MidtransURL:   "",
		CreatedAt:     time.Time{},
		UpdatedAt:     time.Time{},
	}

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			url := fmt.Sprintf("/fundraisings/%d/donations", test.FundraisingID)
			request, _ := http.NewRequest(http.MethodGet, url, nil)
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// Add query param.
			query := request.URL.Query()
			query.Set("page", strconv.Itoa(test.Input.Page))
			query.Set("sort", test.Input.Sort)
			query.Set("order", test.Input.Order)
			query.Set("search", test.Input.Search)
			query.Set("limit", strconv.Itoa(test.Input.Limit))
			request.URL.RawQuery = query.Encode()

			// Program the mock.
			test.Input.Search = fmt.Sprintf("fundraising_id:%d,status:%s,",
				test.FundraisingID, enums.STATUS_SUCCESS)
			donationService.Mock.On("GetPagination", test.Input).Return(donation)

			// Send the request.
			recorder, response, _ := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Parse result to fundraising.
			var donations []models.Donation
			donations = make([]models.Donation, response.ItemsInPage)
			for i, val := range response.Data.([]interface{}) {
				_ = donations[i].FromMapInterface(val.(map[string]interface{}))
			}

			// Assert the response pagination information.
			assert.Equal(t, test.Input.GetLimit(), response.Limit)
			assert.Equal(t, test.Input.GetPage(), response.Page)
			assert.Equal(t, test.Input.GetSort(), response.Sort)
			assert.Equal(t, test.Input.GetOrder(), response.Order)
			assert.Equal(t, test.Input.GetSearch(), response.Search)

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)
			assert.Equal(t, test.Input.GetLimit(), len(donations))
		})
	}
}

func TestDonationHandler_GetMine(t *testing.T) {
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Token      string
		Input      *models.PaginationInput
	}{
		{
			Name:      "Success",
			IsSuccess: true,
			Input: &models.PaginationInput{
				Limit: 10,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:      "Default Limit",
			IsSuccess: true,
			Input: &models.PaginationInput{
				Limit: -1,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:      "Unauthenticated",
			IsSuccess: false,
			Input: &models.PaginationInput{
				Limit: 10,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusUnauthorized,
			Token:      "",
		},
	}

	// The valid donation.
	donation := models.Donation{
		ID:            "1",
		UserID:        userData.User.ID,
		User:          models.UserOutput{},
		FundraisingID: 1,
		Fundraising:   models.FundraisingOutput{},
		Amount:        4200,
		IsAnonymous:   false,
		Status:        "Success",
		MidtransToken: "",
		MidtransURL:   "",
		CreatedAt:     time.Time{},
		UpdatedAt:     time.Time{},
	}

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			request, _ := http.NewRequest(http.MethodGet, "/donations/my", nil)
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// Add query param.
			query := request.URL.Query()
			query.Set("page", strconv.Itoa(test.Input.Page))
			query.Set("sort", test.Input.Sort)
			query.Set("order", test.Input.Order)
			query.Set("search", test.Input.Search)
			query.Set("limit", strconv.Itoa(test.Input.Limit))
			request.URL.RawQuery = query.Encode()

			// Program the mock.
			test.Input.Search = fmt.Sprintf("user_id:%s,", strconv.Itoa(userData.User.ID))
			donationService.Mock.On("GetPagination", test.Input).Return(donation)

			// Send the request.
			recorder, response, _ := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Parse result to fundraising.
			var donations []models.Donation
			donations = make([]models.Donation, response.ItemsInPage)
			for i, val := range response.Data.([]interface{}) {
				_ = donations[i].FromMapInterface(val.(map[string]interface{}))
			}

			// Assert the response pagination information.
			assert.Equal(t, test.Input.GetLimit(), response.Limit)
			assert.Equal(t, test.Input.GetPage(), response.Page)
			assert.Equal(t, test.Input.GetSort(), response.Sort)
			assert.Equal(t, test.Input.GetOrder(), response.Order)
			assert.Equal(t, test.Input.GetSearch(), response.Search)

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)
			assert.Equal(t, test.Input.GetLimit(), len(donations))
		})
	}
}

func TestDonationHandler_GetPagination(t *testing.T) {
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Token      string
		Input      *models.PaginationInput
	}{
		{
			Name:      "Success",
			IsSuccess: true,
			Input: &models.PaginationInput{
				Limit: 10,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:      "Default Limit",
			IsSuccess: true,
			Input: &models.PaginationInput{
				Limit: -1,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
	}

	// The valid donation.
	donation := models.Donation{
		ID:            "1",
		UserID:        userData.User.ID,
		User:          models.UserOutput{},
		FundraisingID: 1,
		Fundraising:   models.FundraisingOutput{},
		Amount:        4200,
		IsAnonymous:   false,
		Status:        "Success",
		MidtransToken: "",
		MidtransURL:   "",
		CreatedAt:     time.Time{},
		UpdatedAt:     time.Time{},
	}

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			request, _ := http.NewRequest(http.MethodGet, "/donations", nil)
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// Add query param.
			query := request.URL.Query()
			query.Set("page", strconv.Itoa(test.Input.Page))
			query.Set("sort", test.Input.Sort)
			query.Set("order", test.Input.Order)
			query.Set("search", test.Input.Search)
			query.Set("limit", strconv.Itoa(test.Input.Limit))
			request.URL.RawQuery = query.Encode()

			// Program the mock.
			donationService.Mock.On("GetPagination", test.Input).Return(donation)

			// Send the request.
			recorder, response, _ := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Parse result to fundraising.
			var donations []models.Donation
			donations = make([]models.Donation, response.ItemsInPage)
			for i, val := range response.Data.([]interface{}) {
				_ = donations[i].FromMapInterface(val.(map[string]interface{}))
			}

			// Assert the response pagination information.
			assert.Equal(t, test.Input.GetLimit(), response.Limit)
			assert.Equal(t, test.Input.GetPage(), response.Page)
			assert.Equal(t, test.Input.GetSort(), response.Sort)
			assert.Equal(t, test.Input.GetOrder(), response.Order)
			assert.Equal(t, test.Input.GetSearch(), response.Search)

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)
			assert.Equal(t, test.Input.GetLimit(), len(donations))
		})
	}
}

func TestDonationHandler_Create(t *testing.T) {
	tests := []struct {
		Name          string
		IsSuccess     bool
		StatusCode    int
		UserData      helpers.TestUserData
		FundraisingID int
		Input         models.DonationCreateInput
	}{
		{
			Name:          "Success",
			IsSuccess:     true,
			FundraisingID: 11,
			Input: models.DonationCreateInput{
				Amount:      4200,
				IsAnonymous: false,
			},
			StatusCode: http.StatusCreated,
			UserData:   userData,
		},
		{
			Name:          "Success Anonymous",
			IsSuccess:     true,
			FundraisingID: 12,
			Input: models.DonationCreateInput{
				Amount:      4200,
				IsAnonymous: true,
			},
			StatusCode: http.StatusCreated,
			UserData:   userData,
		},
		{
			Name:          "Unauthenticated",
			IsSuccess:     false,
			FundraisingID: 11,
			Input: models.DonationCreateInput{
				Amount:      4200,
				IsAnonymous: false,
			},
			StatusCode: http.StatusUnauthorized,
			UserData:   helpers.TestUserData{},
		},
		{
			Name:          "Not Found",
			IsSuccess:     false,
			FundraisingID: 0,
			Input: models.DonationCreateInput{
				Amount:      4200,
				IsAnonymous: false,
			},
			StatusCode: http.StatusNotFound,
			UserData:   userData,
		},
	}

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// The valid donation.
			donation := models.Donation{
				UserID:        test.UserData.User.ID,
				User:          models.UserOutput{},
				FundraisingID: test.FundraisingID,
				Fundraising:   models.FundraisingOutput{},
				Amount:        test.Input.Amount,
				IsAnonymous:   test.Input.IsAnonymous,
			}

			// The valid fundraising.
			fundraising := models.Fundraising{
				ID:             test.FundraisingID,
				FundraiserID:   test.UserData.User.ID,
				Title:          "Test",
				Description:    "Test",
				Receiver:       "Test",
				TargetDonation: 10000,
				TargetDate:     time.Time{},
			}

			// Program the mock.
			if test.FundraisingID <= 0 {
				fundraisingService.Mock.On("GetById", strconv.Itoa(test.FundraisingID)).Return(nil)
				donationService.Mock.On("Create", test.FundraisingID).Return(nil)
			} else {
				fundraisingService.Mock.On("GetById", strconv.Itoa(test.FundraisingID)).Return(fundraising)
				donationService.Mock.On("Create", test.FundraisingID).Return(donation)
			}

			// Create the request.
			reqBody, err := json.Marshal(test.Input)
			url := fmt.Sprintf("/fundraisings/%d/donate", test.FundraisingID)
			request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(reqBody))

			// Add header.
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.UserData.Token))

			// Send the request.
			recorder, response, err := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			respDonation := models.Donation{}
			err = respDonation.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, donation, respDonation)
		})
	}
}
