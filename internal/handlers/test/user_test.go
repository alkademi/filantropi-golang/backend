package test

import (
	"bytes"
	"encoding/json"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"
)

func TestUserHandler_GetById(t *testing.T) {
	// Test case.
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		UserId     int
	}{
		{
			Name:       "Success",
			IsSuccess:  true,
			StatusCode: http.StatusOK,
			UserId:     1,
		},
		{
			Name:       "Not Found",
			IsSuccess:  false,
			StatusCode: http.StatusNotFound,
			UserId:     2,
		},
	}

	// The valid user.
	user := models.User{
		ID:          tests[0].UserId,
		Name:        "Test Surates",
		Email:       "test@test.com",
		PhoneNumber: "01234567890",
		Photo:       "",
		Username:    "test",
		IsVerified:  false,
		Hash:        "",
		Role:        enums.USER_REGULAR,
		Fundraising: nil,
		Donation:    nil,
		CreatedAt:   time.Time{},
		UpdatedAt:   time.Time{},
	}

	// Program the mock.
	userService.Mock.On("GetBy", "id", strconv.Itoa(1)).Return(user)
	userService.Mock.On("GetBy", "id", strconv.Itoa(2)).Return(nil)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			request, _ := http.NewRequest(http.MethodGet,
				fmt.Sprintf("/users/%d", test.UserId), nil)

			// Send the request.
			recorder, response, err := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			resUser := models.User{}
			err = resUser.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, user, resUser)
		})
	}
}

func TestUserHandler_GetMine(t *testing.T) {
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Token      string
	}{
		{
			Name:       "Success",
			IsSuccess:  true,
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:       "Unauthenticated",
			IsSuccess:  false,
			StatusCode: http.StatusUnauthorized,
			Token:      "",
		},
	}

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			request, _ := http.NewRequest(http.MethodGet, "/users/my", nil)
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// Send the request.
			recorder, response, err := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			resUser := models.User{}
			err = resUser.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, userData.User, resUser)
		})
	}
}

func TestUserHandler_Create(t *testing.T) {
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Token      string
	}{
		{
			Name:       "Success",
			IsSuccess:  true,
			StatusCode: http.StatusCreated,
			Token:      adminData.Token,
		},
		{
			Name:       "Forbidden",
			IsSuccess:  false,
			StatusCode: http.StatusForbidden,
			Token:      userData.Token,
		},
		{
			Name:       "Unauthenticated",
			IsSuccess:  false,
			StatusCode: http.StatusUnauthorized,
			Token:      "",
		},
	}

	// Valid user input.
	userInput := models.UserCreateInput{
		Name:        "Test",
		Email:       "test@test.com",
		PhoneNumber: "01234567890",
		Photo:       "",
		Username:    "test_create",
		Password:    "test_create",
	}

	// Hash the password.
	hash, _ := helpers.HashPassword(userInput.Password)
	// Change the user input to match the response data.
	user, _ := userInput.ToUser(hash)
	user.Hash = ""

	// Program the mock.
	userService.Mock.On("Create", &userInput).Return(user)
	userService.Mock.On("GetBy", "username", userInput.Username).Return(nil)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			reqBody, err := json.Marshal(userInput)
			request, err := http.NewRequest(http.MethodPost, "/users/", bytes.NewBuffer(reqBody))
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// A response recorder for getting written http response.
			recorder := httptest.NewRecorder()
			// Send the request.
			r.ServeHTTP(recorder, request)

			// Parse the response.
			response := models.Response{}
			err = json.Unmarshal(recorder.Body.Bytes(), &response)
			assert.NoError(t, err)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			resUser := models.User{}
			err = resUser.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, *user, resUser)
		})
	}
}

func TestUserHandler_UpdateMine(t *testing.T) {
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Token      string
	}{
		{
			Name:       "Success",
			IsSuccess:  true,
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:       "Unauthenticated",
			IsSuccess:  false,
			StatusCode: http.StatusUnauthorized,
			Token:      "",
		},
	}

	// Valid user input.
	userInput := models.UserUpdateInput{
		Name:        "Test",
		Email:       "test@test.com",
		PhoneNumber: "01234567890",
		Photo:       "",
		Password:    "test",
	}

	// Hash the password.
	hash, _ := helpers.HashPassword(userInput.Password)
	// Change the user input to match the response data.
	user, _ := userInput.ToUser(hash)
	user.Hash = ""

	// Program the mock.
	userService.Mock.On("Update", &userInput).Return(user)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			reqBody, err := json.Marshal(userInput)
			request, err := http.NewRequest(http.MethodPut, "/users/my", bytes.NewBuffer(reqBody))
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// A response recorder for getting written http response.
			recorder := httptest.NewRecorder()
			// Send the request.
			r.ServeHTTP(recorder, request)

			// Parse the response.
			response := models.Response{}
			err = json.Unmarshal(recorder.Body.Bytes(), &response)
			assert.NoError(t, err)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			resUser := models.User{}
			err = resUser.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, *user, resUser)
		})
	}
}
