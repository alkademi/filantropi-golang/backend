package test

import (
	"bytes"
	"encoding/json"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func TestFundraisingHandler_GetById(t *testing.T) {
	// Test case.
	tests := []struct {
		Name          string
		IsSuccess     bool
		StatusCode    int
		FundraisingId string
	}{
		{
			Name:          "Success",
			IsSuccess:     true,
			StatusCode:    http.StatusOK,
			FundraisingId: "1",
		},
		{
			Name:          "Not Found",
			IsSuccess:     false,
			StatusCode:    http.StatusNotFound,
			FundraisingId: "2",
		},
	}

	// The fundraising to be validated.
	fundraisingId, _ := strconv.Atoi(tests[0].FundraisingId)
	fundraising := models.Fundraising{
		ID:              fundraisingId,
		FundraiserID:    userData.User.ID,
		Title:           "Test",
		Description:     "Test",
		Receiver:        "Test",
		TargetDonation:  10000,
		TargetDate:      time.Time{},
		Donation:        nil,
		CurrentDonation: 0,
		TakenDonation:   0,
		CountDonor:      0,
		IsActive:        true,
		CreatedAt:       time.Time{},
		UpdatedAt:       time.Time{},
	}

	// Program the mock.
	fundraisingService.Mock.On("GetById", "1").Return(fundraising)
	fundraisingService.Mock.On("GetById", "2").Return(nil)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			request, err := http.NewRequest(http.MethodGet,
				fmt.Sprintf("/fundraisings/%s", test.FundraisingId), nil)

			// Send the request.
			recorder, response, err := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			fundraisingResp := models.Fundraising{}
			err = fundraisingResp.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, fundraising, fundraisingResp)
		})
	}
}

func TestFundraisingHandler_Create(t *testing.T) {
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Token      string
		Input      models.FundraisingCreateInput
	}{
		{
			Name:      "Success",
			IsSuccess: true,
			Input: models.FundraisingCreateInput{
				Title:          "TestCreate",
				Description:    "TestCreate",
				Receiver:       "TestCreate",
				TargetDonation: 8500,
				TargetDate:     time.Now().AddDate(0, 0, 10),
			},
			StatusCode: http.StatusCreated,
			Token:      userData.Token,
		},
		{
			Name:      "Unauthenticated",
			IsSuccess: false,
			Input: models.FundraisingCreateInput{
				Title:          "TestCreate",
				Description:    "TestCreate",
				Receiver:       "TestCreate",
				TargetDonation: 8500,
				TargetDate:     time.Now().AddDate(0, 0, 10),
			},
			StatusCode: http.StatusUnauthorized,
			Token:      "",
		},
		{
			Name:      "Not Valid TargetDate",
			IsSuccess: false,
			Input: models.FundraisingCreateInput{
				Title:          "TestCreate",
				Description:    "TestCreate",
				Receiver:       "TestCreate",
				TargetDonation: 8500,
				TargetDate:     time.Now().AddDate(0, 0, -10),
			},
			StatusCode: http.StatusBadRequest,
			Token:      userData.Token,
		},
		{
			Name:      "Not Valid TargetDonation ",
			IsSuccess: false,
			Input: models.FundraisingCreateInput{
				Title:          "TestCreate",
				Description:    "TestCreate",
				Receiver:       "TestCreate",
				TargetDonation: -8500,
				TargetDate:     time.Now().AddDate(0, 0, 10),
			},
			StatusCode: http.StatusBadRequest,
			Token:      userData.Token,
		},
	}

	// The valid fundraising.
	fundraising := models.Fundraising{
		FundraiserID:   userData.User.ID,
		Title:          tests[0].Input.Title,
		Description:    tests[0].Input.Description,
		Receiver:       tests[0].Input.Receiver,
		TargetDonation: tests[0].Input.TargetDonation,
		TargetDate:     tests[0].Input.TargetDate,
		IsActive:       true,
		CreatedAt:      time.Time{},
		UpdatedAt:      time.Time{},
	}

	// Program the mock.
	fundraisingService.Mock.On("Create", fundraising.FundraiserID).Return(fundraising)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			reqBody, err := json.Marshal(test.Input)
			request, err := http.NewRequest(http.MethodPost, "/fundraisings/", bytes.NewBuffer(reqBody))
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// Send the request.
			recorder, response, err := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			fundraisingResp := models.Fundraising{}
			err = fundraisingResp.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, fundraising, fundraisingResp)
		})
	}
}

func TestFundraisingHandler_GetMine(t *testing.T) {
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Token      string
		Input      *models.PaginationInput
	}{
		{
			Name:      "Success",
			IsSuccess: true,
			Input: &models.PaginationInput{
				Limit: 10,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:      "Default Limit",
			IsSuccess: true,
			Input: &models.PaginationInput{
				Limit: -1,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:      "Unauthenticated",
			IsSuccess: false,
			Input: &models.PaginationInput{
				Limit: 10,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusUnauthorized,
			Token:      "",
		},
	}

	// The fundraising to be validated.
	fundraising := models.Fundraising{
		FundraiserID:    userData.User.ID,
		Title:           "Test",
		Description:     "Test",
		Receiver:        "Test",
		TargetDonation:  10000,
		TargetDate:      time.Time{},
		Donation:        nil,
		CurrentDonation: 0,
		TakenDonation:   0,
		CountDonor:      0,
		IsActive:        true,
		CreatedAt:       time.Time{},
		UpdatedAt:       time.Time{},
	}

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			request, _ := http.NewRequest(http.MethodGet, "/fundraisings/my", nil)
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// Add query param.
			query := request.URL.Query()
			query.Set("page", strconv.Itoa(test.Input.Page))
			query.Set("sort", test.Input.Sort)
			query.Set("order", test.Input.Order)
			query.Set("search", test.Input.Search)
			query.Set("limit", strconv.Itoa(test.Input.Limit))
			request.URL.RawQuery = query.Encode()

			// Program the mock.
			test.Input.Search = fmt.Sprintf("fundraiser_id:%s,", strconv.Itoa(userData.User.ID))
			fundraisingService.Mock.On("GetPaginated", test.Input).Return(fundraising)

			// Send the request.
			recorder, response, _ := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Parse result to fundraising.
			var fundraisings []models.Fundraising
			fundraisings = make([]models.Fundraising, response.ItemsInPage)
			for i, val := range response.Data.([]interface{}) {
				_ = fundraisings[i].FromMapInterface(val.(map[string]interface{}))
			}

			// Assert the response pagination information.
			assert.Equal(t, test.Input.GetLimit(), response.Limit)
			assert.Equal(t, test.Input.GetPage(), response.Page)
			assert.Equal(t, test.Input.GetSort(), response.Sort)
			assert.Equal(t, test.Input.GetOrder(), response.Order)
			assert.Equal(t, test.Input.GetSearch(), response.Search)

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)
			assert.Equal(t, test.Input.GetLimit(), len(fundraisings))
		})
	}
}

func TestFundraisingHandler_GetPaginated(t *testing.T) {
	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Token      string
		Input      *models.PaginationInput
	}{
		{
			Name:      "Success",
			IsSuccess: true,
			Input: &models.PaginationInput{
				Limit: 10,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:      "Default Limit",
			IsSuccess: true,
			Input: &models.PaginationInput{
				Limit: -1,
				Page:  1,
				Sort:  "created_at",
				Order: "desc",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
	}

	// The fundraising to be validated.
	fundraising := models.Fundraising{
		FundraiserID:    userData.User.ID,
		Title:           "Test",
		Description:     "Test",
		Receiver:        "Test",
		TargetDonation:  10000,
		TargetDate:      time.Time{},
		Donation:        nil,
		CurrentDonation: 0,
		TakenDonation:   0,
		CountDonor:      0,
		IsActive:        true,
		CreatedAt:       time.Time{},
		UpdatedAt:       time.Time{},
	}

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			request, _ := http.NewRequest(http.MethodGet, "/fundraisings", nil)
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// Add query param.
			query := request.URL.Query()
			query.Set("page", strconv.Itoa(test.Input.Page))
			query.Set("sort", test.Input.Sort)
			query.Set("order", test.Input.Order)
			query.Set("search", test.Input.Search)
			query.Set("limit", strconv.Itoa(test.Input.Limit))
			request.URL.RawQuery = query.Encode()

			// Program the mock.
			fundraisingService.Mock.On("GetPaginated", test.Input).Return(fundraising)

			// Send the request.
			recorder, response, _ := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Parse result to fundraising.
			var fundraisings []models.Fundraising
			fundraisings = make([]models.Fundraising, response.ItemsInPage)
			for i, val := range response.Data.([]interface{}) {
				_ = fundraisings[i].FromMapInterface(val.(map[string]interface{}))
			}

			// Assert the response pagination information.
			assert.Equal(t, test.Input.GetLimit(), response.Limit)
			assert.Equal(t, test.Input.GetPage(), response.Page)
			assert.Equal(t, test.Input.GetSort(), response.Sort)
			assert.Equal(t, test.Input.GetOrder(), response.Order)
			assert.Equal(t, test.Input.GetSearch(), response.Search)

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)
			assert.Equal(t, test.Input.GetLimit(), len(fundraisings))
		})
	}
}

func TestFundraisingHandler_Update(t *testing.T) {
	tests := []struct {
		Name          string
		IsSuccess     bool
		StatusCode    int
		Token         string
		FundraisingId string
		Input         models.FundraisingUpdateInput
	}{
		{
			Name:          "Success 1",
			IsSuccess:     true,
			FundraisingId: "1",
			Input: models.FundraisingUpdateInput{
				Title:          "TestUpdate",
				Description:    "TestUpdate",
				Receiver:       "TestUpdate",
				TargetDonation: 8500,
				TargetDate:     time.Now().AddDate(0, 0, 10),
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:          "Success 2",
			IsSuccess:     true,
			FundraisingId: "1",
			Input: models.FundraisingUpdateInput{
				Title: "TestUpdate",
			},
			StatusCode: http.StatusOK,
			Token:      userData.Token,
		},
		{
			Name:          "Unauthenticated",
			IsSuccess:     false,
			FundraisingId: "1",
			Input: models.FundraisingUpdateInput{
				Title:          "TestUpdate",
				Description:    "TestUpdate",
				Receiver:       "TestUpdate",
				TargetDonation: 8500,
				TargetDate:     time.Now().AddDate(0, 0, 10),
			},
			StatusCode: http.StatusUnauthorized,
			Token:      "",
		},
		{
			Name:          "Not Valid TargetDate",
			IsSuccess:     false,
			FundraisingId: "1",
			Input: models.FundraisingUpdateInput{
				TargetDate: time.Now().AddDate(0, 0, -10),
			},
			StatusCode: http.StatusBadRequest,
			Token:      userData.Token,
		},
		{
			Name:          "Not Valid TargetDonation ",
			IsSuccess:     false,
			FundraisingId: "1",
			Input: models.FundraisingUpdateInput{
				TargetDonation: -8500,
			},
			StatusCode: http.StatusBadRequest,
			Token:      userData.Token,
		},
		{
			Name:          "Not Found",
			IsSuccess:     false,
			FundraisingId: "2",
			Input: models.FundraisingUpdateInput{
				TargetDonation: 8500,
			},
			StatusCode: http.StatusNotFound,
			Token:      userData.Token,
		},
	}

	// The valid fundraising.
	fundraising := models.Fundraising{
		FundraiserID:   userData.User.ID,
		Title:          "BaseTestUpdate",
		Description:    "BaseTestUpdate",
		Receiver:       "BaseTestUpdate",
		TargetDonation: 97000,
		TargetDate:     time.Now().AddDate(0, 0, 20),
		IsActive:       true,
		CreatedAt:      time.Time{},
		UpdatedAt:      time.Time{},
	}

	// Program the mock.
	fundraisingService.Mock.On("GetById", "1").Return(fundraising)
	fundraisingService.Mock.On("GetById", "2").Return(nil)
	fundraisingService.Mock.On("Update", "1").Return(fundraising)
	fundraisingService.Mock.On("Update", "2").Return(nil)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			reqBody, err := json.Marshal(test.Input)
			url := fmt.Sprintf("/fundraisings/%s", test.FundraisingId)
			request, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(reqBody))
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.Token))

			// Send the request.
			recorder, response, err := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			fundraisingResp := models.Fundraising{}
			err = fundraisingResp.FromMapInterface(response.Data.(map[string]interface{}))

			// Validate the response data.
			assert.NoError(t, err)

			// Validate that every field we update is updated.
			if test.Input.Title != "" {
				assert.Equal(t, test.Input.Title, fundraisingResp.Title)
			}
			if test.Input.Description != "" {
				assert.Equal(t, test.Input.Description, fundraisingResp.Description)
			}
			if test.Input.Receiver != "" {
				assert.Equal(t, test.Input.Receiver, fundraisingResp.Receiver)
			}
			if test.Input.TargetDonation != 0 {
				assert.Equal(t, test.Input.TargetDonation, fundraisingResp.TargetDonation)
			}
			if !test.Input.TargetDate.IsZero() {
				assert.Equal(t, test.Input.TargetDate, fundraisingResp.TargetDate)
			}
		})
	}
}

func TestFundraisingHandler_Close(t *testing.T) {
	tests := []struct {
		Name          string
		IsSuccess     bool
		StatusCode    int
		UserData      helpers.TestUserData
		FundraisingId string
		Input         models.FundraisingCloseInput
	}{
		{
			Name:          "Success",
			IsSuccess:     true,
			FundraisingId: "1",
			Input: models.FundraisingCloseInput{
				Category:    "Bad",
				Description: "TestClose",
			},
			StatusCode: http.StatusOK,
			UserData:   adminData,
		},
		{
			Name:          "Fail already closed",
			IsSuccess:     false,
			FundraisingId: "2",
			Input: models.FundraisingCloseInput{
				Category:    "Bad",
				Description: "TestClose",
			},
			StatusCode: http.StatusBadRequest,
			UserData:   adminData,
		},
		{
			Name:          "Unauthenticated",
			IsSuccess:     false,
			FundraisingId: "1",
			Input: models.FundraisingCloseInput{
				Category:    "Bad",
				Description: "TestClose",
			},
			StatusCode: http.StatusUnauthorized,
			UserData:   helpers.TestUserData{},
		},
		{
			Name:          "Unauthorized",
			IsSuccess:     false,
			FundraisingId: "1",
			Input: models.FundraisingCloseInput{
				Category:    "Bad",
				Description: "TestClose",
			},
			StatusCode: http.StatusForbidden,
			UserData:   userData,
		},
		{
			Name:          "Not found",
			IsSuccess:     false,
			FundraisingId: "3",
			Input: models.FundraisingCloseInput{
				Category:    "Bad",
				Description: "TestClose",
			},
			StatusCode: http.StatusNotFound,
			UserData:   adminData,
		},
	}

	// The valid fundraising_open.
	fundraisingOpen := models.Fundraising{
		FundraiserID:   userData.User.ID,
		Title:          "BaseTestUpdate",
		Description:    "BaseTestUpdate",
		Receiver:       "BaseTestUpdate",
		TargetDonation: 97000,
		TargetDate:     time.Now().AddDate(0, 0, 20),
		IsActive:       true,
		CreatedAt:      time.Time{},
		UpdatedAt:      time.Time{},
	}

	fundraisingClosed := models.Fundraising{
		FundraiserID:   userData.User.ID,
		Title:          "BaseTestUpdate",
		Description:    "BaseTestUpdate",
		Receiver:       "BaseTestUpdate",
		TargetDonation: 97000,
		TargetDate:     time.Now().AddDate(0, 0, 20),
		IsActive:       false,
		CreatedAt:      time.Time{},
		UpdatedAt:      time.Time{},
	}

	// Program the mock.
	fundraisingService.Mock.On("GetById", "1").Return(fundraisingOpen)
	fundraisingService.Mock.On("GetById", "2").Return(fundraisingClosed)
	fundraisingService.Mock.On("GetById", "3").Return(nil)
	fundraisingService.Mock.On("Close", "1").Return(fundraisingOpen)
	fundraisingService.Mock.On("Close", "2").Return(fundraisingClosed)
	fundraisingService.Mock.On("Close", "3").Return(nil)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			reqBody, err := json.Marshal(test.Input)
			url := fmt.Sprintf("/fundraisings/close/%s", test.FundraisingId)
			request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(reqBody))

			// Set the token.
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", test.UserData.Token))

			// Send the request.
			recorder, response, err := SendRequest(t, request)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Parse the response data.
			respCloseReport := models.FundraisingCloseReport{}
			jsonStr, _ := json.Marshal(response.Data.(map[string]interface{}))
			_ = json.Unmarshal(jsonStr, &respCloseReport)

			// Create the expected response.
			closeReport := models.FundraisingCloseReport{}
			fundraisingID, _ := strconv.Atoi(test.FundraisingId)
			_ = closeReport.FromCloseInput(&test.Input, test.UserData.User.ID, fundraisingID)

			// Validate the response data.
			assert.NoError(t, err)
			assert.Equal(t, respCloseReport, closeReport)
		})
	}
}
