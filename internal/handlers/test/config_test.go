package test

import (
	"encoding/json"
	"filantropi/internal/handlers"
	mock3 "filantropi/internal/handlers/mock"
	"filantropi/internal/helpers"
	"filantropi/internal/middleware"
	"filantropi/internal/models"
	"filantropi/internal/routes"
	mock2 "filantropi/internal/services/mock"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

// Gin router.
var r = gin.Default()
var apiGroup = r.Group("/")

// Set up the fundraising service and handler.
var fundraisingService = &mock2.FundraisingServiceMock{Mock: mock.Mock{}}
var fundraisingHandler = handlers.InitFundraisingHandler(fundraisingService)

// Set up the user service and handler.
var donationService = &mock2.DonationServiceMock{Mock: mock.Mock{}}
var donationHandler = handlers.InitDonationHandler(donationService, fundraisingService)

// Set up files handler
var filesHandler = mock3.FileHandlerMock{}

// Set up the user service and jwt middleware.
var userService = &mock2.UserServiceMock{Mock: mock.Mock{}}
var userHandler = handlers.InitUserHandler(userService)
var jwtMiddleware = middleware.JWTMiddleware{UserService: userService}

// Set up the midtrans handler.
var midtransHandler = handlers.MidtransHandler{
	FundraisingService: fundraisingService,
	DonationService:    donationService,
}

// Set up the test user.
var userData = helpers.InitTestUserData()
var adminData = helpers.InitTestAdminData()

// TestMain is the entry point for testing. We can set up the before and after test.
func TestMain(m *testing.M) {
	// Before test.
	gin.SetMode(gin.TestMode)

	// Program the mock for authentication.
	userService.Mock.On("GetBy", "username", userData.User.Username).Return(userData.User)
	userService.Mock.On("GetBy", "id", strconv.Itoa(userData.User.ID)).Return(userData.User)

	userService.Mock.On("GetBy", "username", adminData.User.Username).Return(adminData.User)
	userService.Mock.On("GetBy", "id", strconv.Itoa(adminData.User.ID)).Return(adminData.User)

	// Set the routes group.
	routes.AuthGroup(apiGroup, userHandler)
	routes.UserGroup(apiGroup, &userHandler, jwtMiddleware)
	routes.FundraisingGroup(apiGroup, fundraisingHandler, donationHandler, jwtMiddleware)
	routes.DonationGroup(apiGroup, donationHandler, jwtMiddleware)
	routes.FilesGroup(apiGroup, &filesHandler)
	routes.WebhookGroup(apiGroup, midtransHandler)

	m.Run()

	// After test.
}

// SendRequest is a helper function to send a request to the router.
func SendRequest(t *testing.T, request *http.Request) (*httptest.ResponseRecorder,
	models.ResponsePagination, error) {
	// A response recorder for getting written http response.
	recorder := httptest.NewRecorder()
	// Send the request.
	r.ServeHTTP(recorder, request)

	// Parse the response.
	response := models.ResponsePagination{}
	err := json.Unmarshal(recorder.Body.Bytes(), &response)
	assert.NoError(t, err)
	return recorder, response, err
}
