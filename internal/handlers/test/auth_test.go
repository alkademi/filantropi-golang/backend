package test

import (
	"bytes"
	"encoding/json"
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestUserHandler_Login(t *testing.T) {
	// Valid User.
	username := "test_login"
	password := "test_login"
	hash, err := helpers.HashPassword(password)
	if err != nil {
		return
	}

	user := models.User{
		ID:          101,
		Name:        "Test",
		Email:       "test@test.com",
		PhoneNumber: "0123456789",
		Photo:       "",
		Username:    username,
		IsVerified:  false,
		Hash:        hash,
		Role:        "",
		Fundraising: nil,
		Donation:    nil,
		CreatedAt:   time.Time{},
		UpdatedAt:   time.Time{},
	}

	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Input      models.UserLoginInput
	}{
		{
			Name:       "Success",
			IsSuccess:  true,
			StatusCode: http.StatusAccepted,
			Input: models.UserLoginInput{
				Username: username,
				Password: password,
			},
		},
		{
			Name:       "Failure - Username not found",
			IsSuccess:  false,
			StatusCode: http.StatusUnauthorized,
			Input: models.UserLoginInput{
				Username: "test2",
				Password: password,
			},
		},
		{
			Name:       "Failure - Password not match",
			IsSuccess:  false,
			StatusCode: http.StatusUnauthorized,
			Input: models.UserLoginInput{
				Username: username,
				Password: "test2",
			},
		},
	}

	// Program the mock.
	userService.Mock.On("GetBy", "username", user.Username).Return(user)
	userService.Mock.On("GetBy", "username", "test2").Return(nil)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			reqBody, err := json.Marshal(test.Input)
			request, err := http.NewRequest(http.MethodPost, "/auth/login",
				bytes.NewBuffer(reqBody))

			// A response recorder for getting written http response.
			recorder := httptest.NewRecorder()
			// Send the request.
			r.ServeHTTP(recorder, request)

			// Parse the response.
			response := models.Response{}
			err = json.Unmarshal(recorder.Body.Bytes(), &response)
			assert.NoError(t, err)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Validate the data.
			// Check if token exist.
			assert.NotNil(t, response.Data.(map[string]interface{})["token"])
		})
	}
}

func TestUserHandler_Register(t *testing.T) {
	// Valid User.
	username := "test_register"
	password := "test_register"

	createInput1 := models.UserCreateInput{
		Name:        "Test_Register",
		PhoneNumber: "01234567891",
		Photo:       "default_profile.jpg",
		Username:    username,
		Password:    password,
	}

	createInput2 := models.UserCreateInput{
		Name:        userData.User.Name,
		PhoneNumber: userData.User.PhoneNumber,
		Photo:       "default_profile.jpg",
		Username:    userData.User.Username,
		Password:    password,
	}

	tests := []struct {
		Name       string
		IsSuccess  bool
		StatusCode int
		Input      models.UserRegisterInput
	}{
		{
			Name:       "Success",
			IsSuccess:  true,
			StatusCode: http.StatusCreated,
			Input: models.UserRegisterInput{
				Name:        createInput1.Name,
				PhoneNumber: createInput1.PhoneNumber,
				Username:    createInput1.Username,
				Password:    createInput1.Password,
			},
		},
		{
			Name:       "Failure - Username already exist",
			IsSuccess:  false,
			StatusCode: http.StatusBadRequest,
			Input: models.UserRegisterInput{
				Name:        createInput2.Name,
				PhoneNumber: createInput2.PhoneNumber,
				Username:    createInput2.Username,
				Password:    createInput2.Password,
			},
		},
	}

	// Program the mock.
	userService.Mock.On("Create", &createInput1).Return(createInput1)
	userService.Mock.On("Create", &createInput2).Return(createInput2)
	userService.Mock.On("GetBy", "username", username).Return(nil)
	userService.Mock.On("GetBy", "username", userData.User.Username).Return(userData.User)

	// Run all test.
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			// Create the request.
			reqBody, err := json.Marshal(test.Input)
			request, err := http.NewRequest(http.MethodPost, "/auth/register",
				bytes.NewBuffer(reqBody))

			// A response recorder for getting written http response.
			recorder := httptest.NewRecorder()
			// Send the request.
			r.ServeHTTP(recorder, request)

			// Parse the response.
			response := models.Response{}
			err = json.Unmarshal(recorder.Body.Bytes(), &response)
			assert.NoError(t, err)

			// Validate the response.
			assert.Equal(t, test.StatusCode, recorder.Code)

			// Fail test.
			if !test.IsSuccess {
				assert.Nil(t, response.Data)
				assert.NotNil(t, response.Error)
				return
			}

			// Success test.
			assert.NotNil(t, response.Data)
			assert.Nil(t, response.Error)

			// Validate the data.
			// Check if token exist.
			assert.NotNil(t, response.Data.(map[string]interface{})["token"])
		})
	}
}
