package handlers

import (
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/services"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// IUserHandler is an interface for UserHandler
type IUserHandler interface {
	GetById(c *gin.Context)
	GetMine(c *gin.Context)
	Create(c *gin.Context)
	UpdateMine(c *gin.Context)
	Delete(c *gin.Context)
	Register(c *gin.Context)
	Login(c *gin.Context)
}

// UserHandler is a handler struct for convenience naming.
type UserHandler struct {
	Service services.IUserService
}

// InitUserHandler is a function for initialize the UserHandler
func InitUserHandler(services services.IUserService) UserHandler {
	return UserHandler{
		Service: services,
	}
}

// GetById is a handler function for getting a user in application by its Id.
// API GET /users/:id
func (h *UserHandler) GetById(c *gin.Context) {
	response := &models.Response{}
	response.Init()

	// Call the userService to get the user.
	_, response, _ = h.Service.GetBy("id", c.Param("id"))
	response.Message = "Successfully fetching a user by its id"

	// Return response message.
	c.JSON(response.StatusCode, response)
}

func (h *UserHandler) GetMine(c *gin.Context) {
	// Initialize variables.
	response := &models.Response{}
	response.Init()

	// Get the user information from context.
	user, response, err := helpers.AuthenticateUser(c)
	if err != nil {
		c.JSON(response.StatusCode, response)
		return
	}

	// Return user information.
	response.Success(
		"Successfully retrieve User information",
		200,
		user)
	c.JSON(response.StatusCode, response)
}

// Create is a handler function for creating a new user.
// API POST /users
func (h *UserHandler) Create(c *gin.Context) {
	input := models.UserCreateInput{}
	response := &models.Response{}

	// Bind the input data.
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response.BadRequest(
			"Failed to create a new user",
			"body",
			"Input is not valid")
		c.JSON(response.StatusCode, response)
		return
	}

	// Call the userService to create a new user.
	_, response, _ = h.Service.Create(&input)

	// Return response message.
	c.JSON(response.StatusCode, response)
}

// UpdateMine is a handler function for update an existing user.
// This route must be authorized.
// API PUT /users/my
func (h *UserHandler) UpdateMine(c *gin.Context) {
	input := models.UserUpdateInput{}
	response := &models.Response{}

	// Bind the input data.
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response.BadRequest(
			"Failed to update the user",
			"body",
			"Input is not valid")
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the User from context.
	user, response, err := helpers.AuthenticateUser(c)
	if err != nil {
		c.JSON(response.StatusCode, response)
		return
	}

	// Call the userService to create a new user.
	_, response, _ = h.Service.Update(user.ID, &input)

	// Return response message.
	c.JSON(response.StatusCode, response)
}

// Delete is a handler function for delete an existing user.
// Only Admin can access this route.
// API DELETE /users/:id
func (h *UserHandler) Delete(c *gin.Context) {
	response := &models.Response{}
	response.Init()

	// Call the userService to delete the user.
	_, response, _ = h.Service.Delete(c.Param("id"))

	// Return response message.
	c.JSON(response.StatusCode, response)
}

func (h *UserHandler) Register(c *gin.Context) {
	var input models.UserRegisterInput

	response := &models.Response{}
	response.Init()
	// Failsafe
	response.StatusCode = http.StatusInternalServerError
	response.Message = "Something went wrong in server, register failed"

	// Validate request body
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response.BadRequest("Some data missing from request body", "Body", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Copy data to UserCreateInput structure
	user := models.UserCreateInput{
		Name:        input.Name,
		PhoneNumber: input.PhoneNumber,
		Photo:       "default_profile.jpg",
		Username:    input.Username,
		Password:    input.Password,
	}

	// Call create user userService
	_, response, _ = h.Service.Create(&user)

	// Check response
	if response.StatusCode != 201 {
		c.JSON(response.StatusCode, response)
		return
	}

	// Generate Token
	token := helpers.JWTAuthService().GenerateToken(user.Username, true)

	// Attach token
	response.Data = map[string]interface{}{
		"token": token,
	}

	c.JSON(response.StatusCode, response)
}

func (h *UserHandler) Login(c *gin.Context) {
	var userInput models.UserLoginInput
	response := &models.Response{}

	// Validate the input
	response.Init()
	response.Message = "Login Failed"

	err := c.ShouldBindJSON(&userInput)
	if err != nil {
		response.StatusCode = http.StatusBadRequest
		response.BadRequest("Some data missing from request body", "Body", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Search the database for username
	user, response, err := h.Service.GetBy("username", userInput.Username)
	if err != nil {
		response.Data = nil
		response.StatusCode = http.StatusUnauthorized
		response.Error = &models.ResponseError{
			Location: "param",
			Detail:   "Username or Password is Invalid",
		}
		c.JSON(response.StatusCode, response)
		return
	}

	// Validate the Password
	valid := helpers.CheckHashPassword(userInput.Password, user.Hash)
	if !valid {
		response.Data = nil
		response.StatusCode = http.StatusUnauthorized
		response.Error = &models.ResponseError{
			Location: "param",
			Detail:   "Username or Password is Invalid",
		}
		c.JSON(response.StatusCode, response)
		return
	}

	// Generate Token
	var isUser = true
	if user.Role == "admin" {
		isUser = false
	}
	token := helpers.JWTAuthService().GenerateToken(user.Username, isUser)

	// Create response.
	response.StatusCode = http.StatusAccepted
	response.Message = "Login Success"

	response.Data = map[string]interface{}{
		"token":  token,
		"isUser": isUser,
	}

	c.Header("Authorization", fmt.Sprintf("Bearer %s", token))
	c.JSON(response.StatusCode, response)
}
