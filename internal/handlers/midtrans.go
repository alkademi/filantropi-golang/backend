package handlers

import (
	"filantropi/configs"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"filantropi/internal/services"
	"github.com/gin-gonic/gin"
	"strconv"
	"strings"
)

// MidtransHandler is a struct for wrap the handler and userService.
type MidtransHandler struct {
	FundraisingService services.IFundraisingService
	DonationService    services.IDonationService
}

// DonationNotification is a method of MidtransHandler for handling the notification of Donation
// payment from Midtrans.
// HTTP POST /webhooks/midtrans
func (h *MidtransHandler) DonationNotification(c *gin.Context) {
	// Initialize response.
	response := &models.Response{}

	// Get the payload from context.
	payloadInterface, exist := c.Get(configs.CONTEXT_MIDTRANS_PAYLOAD)
	if !exist {
		response.Fail(
			"[Handler] Cannot parse the request body",
			500,
			nil)
		c.AbortWithStatusJSON(response.StatusCode, response)
		return
	}
	payload := payloadInterface.(map[string]interface{})

	// Get important payload field.
	orderId := payload["order_id"].(string)
	transactionStatus := payload["transaction_status"].(string)
	fraudStatus := payload["fraud_status"].(string)
	statusCode := payload["status_code"].(string)

	// Remove prefix from orderId.
	donationId := strings.TrimPrefix(orderId, configs.MIDTRANS_ORDER_PREFIX)

	// Check if the donation is exists.
	donation, response, err := h.DonationService.GetById(donationId)
	if err != nil {
		// Return the response message.
		c.JSON(response.StatusCode, response)
		return
	}

	// Validate that donation status still pending or challenge.
	if !(donation.Status == enums.STATUS_PENDING || donation.Status == enums.STATUS_CHALLENGE) {
		response.Fail(
			"Transaction is already success or fail",
			200,
			nil)
		c.JSON(response.StatusCode, response)
		return
	}

	// Keep the state of transaction to donation.
	status := enums.STATUS_PENDING
	isValid := true

	// Check the transaction status.
	switch transactionStatus {
	case "capture":
		if fraudStatus == "challenge" {
			status = enums.STATUS_CHALLENGE
		} else if fraudStatus == "accept" && statusCode == "200" {
			status = enums.STATUS_SUCCESS
		} else {
			isValid = false
		}
	case "settlement":
		if statusCode == "200" {
			status = enums.STATUS_SUCCESS
		} else {
			isValid = false
		}
	case "cancel", "deny", "expire":
		status = enums.STATUS_FAILURE
	case "pending":
		status = enums.STATUS_PENDING
	default:
		isValid = false
	}

	// If the switch statement is not valid, make midtrans send again the notification.
	if !isValid {
		response.Fail(
			"The notification is not valid",
			400,
			nil)
		c.JSON(response.StatusCode, response)
		return
	}

	// Update the donation status.
	_, response, err = h.DonationService.UpdateStatus(donationId, enums.DonationStatus(status))
	if err != nil {
		c.JSON(response.StatusCode, response)
		return
	}

	// After successfully update donation, update the fundraising.
	if status == enums.STATUS_SUCCESS {
		_, response, err = h.FundraisingService.UpdateDonation(strconv.Itoa(donation.FundraisingID),
			donation.Amount)
		if err != nil {
			response.Fail(
				"Cannot update the fundraising",
				500,
				nil)
			c.JSON(response.StatusCode, response)
			return
		}
	}

	// Return success response.
	response.Success(
		"Successfully handle the notification",
		200,
		nil)
	c.JSON(response.StatusCode, response)
	return
}
