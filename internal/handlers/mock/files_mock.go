package mock

import (
	"filantropi/configs"
	"filantropi/internal/models"
	"fmt"
	"github.com/gin-gonic/gin"
	uuid2 "github.com/google/uuid"
	"strings"
)

// FileHandlerMock is a handler struct for convenience naming.
type FileHandlerMock struct {
}

// UploadImage is a method of FileHandler for uploading the image from multipart/form-data
// The attribute name in input tag must be configs.FILES_IMAGE_FORM_NAME
func (*FileHandlerMock) UploadImage(c *gin.Context) {
	// Initialize variables.
	response := models.Response{}

	// Input Type File.
	file, err := c.FormFile(configs.FILES_IMAGE_FORM_NAME)

	// Check if there is an error.
	if err != nil {
		response.BadRequest(
			fmt.Sprintf("The name attribute of form input tag must be '%s'", configs.FILES_IMAGE_FORM_NAME),
			"form input name",
			"the name attribute in form input is not valid")
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the file extension.
	splitFilename := strings.Split(file.Filename, ".")
	fileExt := splitFilename[len(splitFilename)-1]

	// Generate uuid for the name of image.
	uuid := uuid2.New()
	id := strings.Replace(uuid.String(), "-", "", -1)

	// Set the name file using unique id.
	name := fmt.Sprintf("%s.%s", id, fileExt)

	// Set the folder path for save the image.
	//path := fmt.Sprintf("./assets/images/%s", name)

	// Return the success response.
	response.Success(
		"Successfully upload the image",
		201,
		models.ResponseDataFile{Name: name})
	c.JSON(response.StatusCode, response)
}
