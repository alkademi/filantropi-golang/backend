package handlers

import (
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"filantropi/internal/services"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

// IDonationHandler is an interface for DonationHandler.
type IDonationHandler interface {
	GetById(c *gin.Context)
	GetByFundraisingId(c *gin.Context)
	GetMine(c *gin.Context)
	GetPagination(c *gin.Context)
	Create(c *gin.Context)
}

// DonationHandler is a struct for wrap the handler and userService.
type DonationHandler struct {
	DonationService    services.IDonationService
	FundraisingService services.IFundraisingService
}

// InitDonationHandler is a function for initialize the DonationHandler.
func InitDonationHandler(
	donationService services.IDonationService,
	fundraisingService services.IFundraisingService,
) DonationHandler {
	return DonationHandler{
		DonationService:    donationService,
		FundraisingService: fundraisingService,
	}
}

// GetById is a method of DonationHandler for get the Donation by its Id.
// HTTP GET /donations/:id
func (h *DonationHandler) GetById(c *gin.Context) {
	// Initialize variables.
	response := &models.Response{}

	// Call the userService to get Fundraising by its id.
	_, response, _ = h.DonationService.GetById(c.Param("id"))

	// Return the response message.
	c.JSON(response.StatusCode, response)
}

// GetByFundraisingId is a method of DonationHandler to get the models.Donation by its Fundraising Id.
// HTTP GET /fundraisings/:id/donations
func (h *DonationHandler) GetByFundraisingId(c *gin.Context) {
	// Initialize variables.
	response := &models.ResponsePagination{}
	input := &models.PaginationInput{}

	// Validate the query input.
	err := c.ShouldBindQuery(input)
	if err != nil {
		response.BadRequest("Cannot get the Donation, Input is not Valid",
			"query",
			err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Insert the fundraising Id in search.
	search := fmt.Sprintf("fundraising_id:%s,status:%s", c.Param("id"), enums.STATUS_SUCCESS)
	input.Search = fmt.Sprintf("%s,%s", search, input.GetSearch())

	// Call the userService to get paginated Fundraising.
	_, response, _ = h.DonationService.GetPagination(input)

	// Return the response message.
	c.JSON(response.StatusCode, response)
}

// GetMine is a method of DonationHandler to get the models.Donation by its User Id.
// The user must be authenticated first.
// HTTP GET /users/donations
func (h *DonationHandler) GetMine(c *gin.Context) {
	// Initialize variables.
	response := &models.ResponsePagination{}
	input := &models.PaginationInput{}

	// Validate the query input.
	err := c.ShouldBindQuery(input)
	if err != nil {
		response.BadRequest("Cannot get the Donation, Input is not Valid",
			"query",
			err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the user data from context.
	user, authResponse, err := helpers.AuthenticateUser(c)
	if err != nil {
		response.Parse(authResponse)
		c.JSON(response.StatusCode, response)
		return
	}

	// Insert the fundraising Id in search.
	search := fmt.Sprintf("user_id:%s", strconv.Itoa(user.ID))
	input.Search = fmt.Sprintf("%s,%s", search, input.GetSearch())

	// Call the userService to get paginated Fundraising.
	_, response, _ = h.DonationService.GetPagination(input)

	// Return the authResponse message.
	c.JSON(response.StatusCode, response)
}

// GetPagination is a method of DonationHandler to get the models.Donation with pagination.
// HTTP GET /donations
func (h *DonationHandler) GetPagination(c *gin.Context) {
	// Initialize variables.
	response := &models.ResponsePagination{}
	input := &models.PaginationInput{}

	// Validate the query input.
	err := c.ShouldBindQuery(input)
	if err != nil {
		response.BadRequest("Cannot get the Donation, input is not Valid",
			"query",
			err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Call the userService to get paginated Fundraising.
	_, response, _ = h.DonationService.GetPagination(input)

	// Return the authResponse message.
	c.JSON(response.StatusCode, response)
}

// Create is a method of DonationHandler for create a new Donation.
// HTTP POST /fundraisings/:fundraisingId/donate
func (h *DonationHandler) Create(c *gin.Context) {
	// Initialize variables.
	input := &models.DonationCreateInput{}
	response := &models.Response{}

	// Validate the input by binding the JSON input body.
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response.BadRequest("Cannot create a new Donation, input is not valid",
			"body", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Validate that input amount must be positive.
	err = input.Validate()
	if err != nil {
		response.BadRequest("Cannot create a new Donation, input is not valid",
			"body", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the fundraisingId.
	fundraisingId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response.BadRequest("The fundraisingId in path parameter is not valid", "param",
			"Input is not valid")
		c.JSON(response.StatusCode, response)
		return
	}

	// Validate that fundraising is existed.
	_, response, err = h.FundraisingService.GetById(strconv.Itoa(fundraisingId))
	if err != nil {
		response.NotFound("The fundraising is not exist", "Param", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the User from context.
	user, response, err := helpers.AuthenticateUser(c)
	if err != nil {
		c.JSON(response.StatusCode, response)
		return
	}

	// Call the userService for creating a new Fundraising.
	_, response, _ = h.DonationService.Create(fundraisingId, input, user)

	// Return the response message.
	c.JSON(response.StatusCode, response)
}
