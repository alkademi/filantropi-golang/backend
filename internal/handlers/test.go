package handlers

import (
	database "filantropi/internal/databases"
	"filantropi/internal/models"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type TestHandler struct{}

func (t *TestHandler) TestHandler(c *gin.Context) {
	// Initialize variables.
	response := &models.ResponsePagination{}
	input := &models.PaginationInput{}

	// Validate the query input.
	err := c.ShouldBindQuery(input)
	if err != nil {
		response.BadRequest("Cannot get the Donation, Input is not Valid",
			"query",
			err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Test the query.
	var donation []models.Donation
	//var result []map[string]interface{}
	//database.DB.Model(&donation).
	//	Where("status = ?", "success").
	//	Offset(2).
	//	Limit(5).
	//	Order("amount desc").
	//	Preload(clause.Associations).
	//	Find(&result)
	query := t.passInterface(donation)
	query.Preload(clause.Associations).Find(&donation)

	c.JSON(200, donation)
	return
}

func (t TestHandler) passInterface(model interface{}) *gorm.DB {
	query := database.DB.Model(&model).
		Where("status = ?", "success").
		Offset(2).
		Limit(5).
		Order("amount asc")

	return query
}
