package handlers

import (
	"filantropi/internal/helpers"
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"filantropi/internal/services"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

// IFundraisingHandler is an interface for the fundraising handler.
type IFundraisingHandler interface {
	GetById(c *gin.Context)
	GetPaginated(c *gin.Context)
	GetMine(c *gin.Context)
	Create(c *gin.Context)
	Update(c *gin.Context)
	Close(c *gin.Context)
}

// FundraisingHandler is a struct for handling fundraising services.
type FundraisingHandler struct {
	Service services.IFundraisingService
}

// InitFundraisingHandler is a function for initialize the FundraisingHandler.
func InitFundraisingHandler(services services.IFundraisingService) FundraisingHandler {
	return FundraisingHandler{
		Service: services,
	}
}

// GetById is a method of FundraisingHandler for fetching a Fundraising by its id.
func (h *FundraisingHandler) GetById(c *gin.Context) {
	// Initialize variables.
	response := &models.Response{}

	// Call the userService to get Fundraising by its id.
	_, response, _ = h.Service.GetById(c.Param("id"))

	// Return the response message.
	c.JSON(response.StatusCode, response)
}

// GetPaginated is a method of FundraisingHandler for getting Fundraising with pagination.
func (h *FundraisingHandler) GetPaginated(c *gin.Context) {
	// Initialize variables.
	response := &models.ResponsePagination{}
	input := &models.PaginationInput{}

	// Validate the query input.
	err := c.ShouldBindQuery(input)
	if err != nil {
		response.BadRequest(
			"Failed to get Fundraisings",
			"query",
			err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Call the userService to get paginated Fundraising.
	_, response, _ = h.Service.GetPaginated(input)

	// Return the response message.
	c.JSON(response.StatusCode, response)
}

// GetMine is a method of FundraisingHandler for get the models.Fundraising that created by models.User
// The User must be Authenticated.
func (h *FundraisingHandler) GetMine(c *gin.Context) {
	// Initialize variables.
	paginationResponse := &models.ResponsePagination{}
	input := &models.PaginationInput{}

	// Validate the query input.
	err := c.ShouldBindQuery(input)
	if err != nil {
		paginationResponse.BadRequest(
			"Failed to get my Fundraising",
			"query",
			err.Error())
		c.JSON(paginationResponse.StatusCode, paginationResponse)
		return
	}

	// Get the user data from context.
	user, response, err := helpers.AuthenticateUser(c)
	if err != nil {
		paginationResponse.Parse(response)
		c.JSON(paginationResponse.StatusCode, paginationResponse)
		return
	}

	// Change the input search variable.
	search := fmt.Sprintf("fundraiser_id:%d", user.ID)
	input.Search = fmt.Sprintf("%s,%s", search, input.GetSearch())

	// Call the userService to get paginated Fundraising.
	_, paginationResponse, _ = h.Service.GetPaginated(input)

	// Return the response message.
	c.JSON(paginationResponse.StatusCode, paginationResponse)
}

// Create is a method of FundraisingHandler for creating a new Fundraising.
// API POST /fundraising
func (h *FundraisingHandler) Create(c *gin.Context) {
	// Initialize variables.
	input := &models.FundraisingCreateInput{}
	response := &models.Response{}

	// Validate the input by binding the JSON input body.
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response.BadRequest(
			"Failed to create a new Fundraising, input is not valid",
			"body", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Validate the input value.
	err = input.Validate()
	if err != nil {
		response.BadRequest(
			"Failed to create a new Fundraising, input is not valid",
			"body", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the User from context.
	user, response, err := helpers.AuthenticateUser(c)
	if err != nil {
		c.JSON(response.StatusCode, response)
		return
	}

	// Call the userService for creating a new Fundraising.
	_, response, _ = h.Service.Create(user.ID, input)

	// Return the response message.
	c.JSON(response.StatusCode, response)
}

// Update is a method of FundraisingHandler for creating a new Fundraising.
// API PUT /fundraising/:id
func (h *FundraisingHandler) Update(c *gin.Context) {
	// Initialize variables.
	input := &models.FundraisingUpdateInput{}
	response := &models.Response{}

	// Validate the input by binding the JSON input body.
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response.BadRequest(
			"Failed to update Fundraising, input is not valid",
			"body",
			err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Validate the input value.
	err = input.Validate()
	if err != nil {
		response.BadRequest(
			"Failed to update Fundraising, input is not valid",
			"body", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the User from context.
	user, response, err := helpers.AuthenticateUser(c)
	if err != nil {
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the fundraising.
	fundraising, response, err := h.Service.GetById(c.Param("id"))
	if err != nil {
		c.JSON(response.StatusCode, response)
		return
	}

	// Validate that fundraiserID is same as userID
	if fundraising.FundraiserID != user.ID {
		response.Unauthorized("You are not the creator of fundraising", "", c)
		c.JSON(response.StatusCode, response)
		return
	}

	// Call the userService for creating a new Fundraising.
	_, response, _ = h.Service.Update(c.Param("id"), input)

	// Return the response message.
	c.JSON(response.StatusCode, response)
}

// Close is a method of FundraisingHandler for closing a Fundraising.
// API PUT /fundraisings/close/:id
func (h *FundraisingHandler) Close(c *gin.Context) {
	//Initialize variables
	user := &models.User{}
	input := &models.FundraisingCloseInput{}
	response := &models.Response{}

	// Validate the input by binding the JSON input body.
	err := c.ShouldBindJSON(&input)
	if err != nil {
		response.BadRequest("Cannot close the Fundraising, input is not valid",
			"body", err.Error())
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the User from context.
	user, response, err = helpers.AuthenticateUser(c)
	if err != nil {
		c.JSON(response.StatusCode, response)
		return
	}

	// Validate that user role is admin
	if user.Role != enums.USER_ADMIN {
		response.Unauthorized("You are not authorized to close this fundraising", "", c)
		c.JSON(response.StatusCode, response)
		return
	}

	// Get the fundraisingId.
	fundraisingID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response.BadRequest("The fundraisingId in path parameter is not valid", "param",
			"Input is not valid")
		c.JSON(response.StatusCode, response)
		return
	}

	// Call the userService for creating a new Fundraising.
	_, response, _ = h.Service.Close(c.Param("id"), input, user.ID, fundraisingID)

	// Return the response message.
	c.JSON(response.StatusCode, response)
}
