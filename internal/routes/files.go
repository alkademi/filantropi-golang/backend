package routes

import (
	"filantropi/internal/handlers"
	"github.com/gin-gonic/gin"
)

// Files is a route group for files related handler.
func Files(route *gin.RouterGroup) {
	fileHandler := handlers.FileHandler{}

	FilesGroup(route, &fileHandler)
}

func FilesGroup(route *gin.RouterGroup, fileHandler handlers.IFileHandler) {
	// Files related route.
	r := route.Group("/files")
	{
		r.POST("/images", fileHandler.UploadImage)
		r.Static("/images", "./assets/images")
	}
}
