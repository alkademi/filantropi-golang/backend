package routes

import (
	"filantropi/internal/handlers"
	"filantropi/internal/middleware"
	"filantropi/internal/services"

	"github.com/gin-gonic/gin"
)

// Fundraising is a route for fundraising related handler.
func Fundraising(route *gin.RouterGroup) {
	fundraisingHandler := handlers.InitFundraisingHandler(&services.FundraisingService{})
	donationHandler := handlers.InitDonationHandler(&services.DonationService{}, &services.FundraisingService{})
	jwtMiddleware := middleware.InitJWTMiddleware()

	FundraisingGroup(route, fundraisingHandler, donationHandler, jwtMiddleware)
}

// FundraisingGroup is a route group for fundraising related handler.
func FundraisingGroup(
	route *gin.RouterGroup,
	fundraisingHandler handlers.FundraisingHandler,
	donationHandler handlers.DonationHandler,
	jwtMiddleware middleware.JWTMiddleware,
) {
	r := route.Group("/fundraisings")
	{
		// Unprotected Routes.
		r.GET("/:id", fundraisingHandler.GetById)
		r.GET("", fundraisingHandler.GetPaginated)

		// Related to donation.
		r.GET("/:id/donations", donationHandler.GetByFundraisingId)

		// Protected Routes.
		rAuth := r.Group("/", jwtMiddleware.Authorize())
		{
			rAuth.GET("/my", fundraisingHandler.GetMine)
			rAuth.POST("", fundraisingHandler.Create)
			rAuth.PUT("/:id", fundraisingHandler.Update)

			// Related to donation.
			rAuth.POST("/:id/donate", donationHandler.Create)
		}

		// Protected Admin Routes.
		rAdmin := r.Group("/", jwtMiddleware.AuthorizeAdmin())
		{
			rAdmin.POST("/close/:id", fundraisingHandler.Close)
		}
	}
}
