package routes

import (
	"filantropi/internal/handlers"
	"github.com/gin-gonic/gin"
)

// Test is a route group for test some API.
func Test(route *gin.RouterGroup) {
	handlerUser := handlers.TestHandler{}

	r := route.Group("/test")
	{
		r.GET("/", handlerUser.TestHandler)
	}
}
