package routes

import (
	"filantropi/internal/handlers"
	"filantropi/internal/middleware"
	"filantropi/internal/services"
	"github.com/gin-gonic/gin"
)

// User is a route group for user related handler.
func User(route *gin.RouterGroup) {
	userHandler := handlers.InitUserHandler(&services.UserService{})
	jwtMiddleware := middleware.InitJWTMiddleware()

	UserGroup(route, &userHandler, jwtMiddleware)
}

// UserGroup is a route group for user related handler.
func UserGroup(route *gin.RouterGroup,
	userHandler handlers.IUserHandler,
	jwtMiddleware middleware.JWTMiddleware,
) {
	r := route.Group("/users")
	{
		// Unprotected Routes.
		r.GET("/:id", userHandler.GetById)

		// Protected Routes.
		rAuth := r.Group("/", jwtMiddleware.Authorize())
		{
			rAuth.GET("/my", userHandler.GetMine)
			rAuth.PUT("/my", userHandler.UpdateMine)
		}

		// Admin Protected Routes.
		rAdmin := r.Group("/", jwtMiddleware.AuthorizeAdmin())
		{
			rAdmin.POST("", userHandler.Create)
		}
	}
}
