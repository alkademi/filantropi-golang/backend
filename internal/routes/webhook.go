package routes

import (
	"filantropi/internal/handlers"
	"filantropi/internal/middleware"
	"filantropi/internal/services"
	"github.com/gin-gonic/gin"
)

// Webhook is a route group for API Webhook.
func Webhook(route *gin.RouterGroup) {
	midtransHandler := handlers.MidtransHandler{
		FundraisingService: &services.FundraisingService{},
		DonationService:    &services.DonationService{},
	}
	WebhookGroup(route, midtransHandler)
}

// WebhookGroup is a route group for Webhook API.
func WebhookGroup(route *gin.RouterGroup, midtransHandler handlers.MidtransHandler) {
	r := route.Group("/webhooks/midtrans", middleware.VerifyMidtransNotification())
	{
		r.POST("/", midtransHandler.DonationNotification)
	}
}
