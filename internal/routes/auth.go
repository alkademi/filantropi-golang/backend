package routes

import (
	"filantropi/internal/handlers"
	"filantropi/internal/services"
	"github.com/gin-gonic/gin"
)

// Auth is a route group for authentication related handler.
func Auth(route *gin.RouterGroup) {
	userHandler := handlers.InitUserHandler(&services.UserService{})

	AuthGroup(route, userHandler)
}

// AuthGroup is a route group for authentication related handler.
func AuthGroup(route *gin.RouterGroup, userHandler handlers.UserHandler) {
	r := route.Group("/auth")
	{
		r.POST("/login", userHandler.Login)
		r.POST("/register", userHandler.Register)
	}
}
