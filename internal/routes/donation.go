package routes

import (
	"filantropi/internal/handlers"
	"filantropi/internal/middleware"
	"filantropi/internal/services"
	"github.com/gin-gonic/gin"
)

// Donation is a route for donation related handler.
func Donation(route *gin.RouterGroup) {
	donationHandler := handlers.InitDonationHandler(&services.DonationService{}, &services.FundraisingService{})
	jwtMiddleware := middleware.InitJWTMiddleware()

	DonationGroup(route, donationHandler, jwtMiddleware)
}

// DonationGroup is a route group for donation related handler.
func DonationGroup(
	route *gin.RouterGroup,
	donationHandler handlers.DonationHandler,
	jwtMiddleware middleware.JWTMiddleware,
) {
	r := route.Group("/donations")
	{
		// Unprotected Routes.
		r.GET("/:id", donationHandler.GetById)
		r.GET("", donationHandler.GetPagination)

		// Protected Routes.
		rAuth := r.Group("/", jwtMiddleware.Authorize())
		{
			rAuth.GET("/my", donationHandler.GetMine)
		}
	}
}
