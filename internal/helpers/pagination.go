package helpers

import (
	"errors"
	"filantropi/configs"
	database "filantropi/internal/databases"
	"filantropi/internal/models"
	"fmt"
	"gorm.io/gorm"
	"strings"
)

// PaginationQuery is a helper function to build a gorm query for pagination.
func PaginationQuery(input *models.PaginationInput, model interface{}) (*gorm.DB, int64, error) {
	// Initialize variables.
	var totalItems int64

	// Build the query.
	queryBuilder := database.DB.Model(model)

	// Validate sort attribute is exist.
	if !database.DB.Migrator().HasColumn(&model, input.GetSort()) {
		return nil, 0, errors.New(fmt.Sprintf("Pagination: sort attribute '%s' not exist",
			input.Sort))
	}

	// Validate order attribute is correct.
	if !(input.GetOrder() == "asc" || input.GetOrder() == "desc") {
		return nil, 0, errors.New(fmt.Sprintf("Pagination: order attribute '%s' not correct",
			input.Order))
	}

	// Check if search query is used and its type.
	if input.GetSearch() != configs.PAGINATION_SEARCH {
		// Split the query by ","
		queries := strings.Split(input.GetSearch(), ",")

		// Iterate each query.
		for _, query := range queries {
			// Get the search query.
			s := strings.Split(query, ":")
			if len(s) != 2 {
				continue
			}
			attr, val := s[0], s[1]

			// Check if attribute exist.
			if !database.DB.Migrator().HasColumn(&model, attr) {
				return nil, 0, errors.New(fmt.Sprintf("Pagination: search attribute '%s' not exist",
					attr))
			}

			// Check the val's data type.
			if isInt(val) || isBoolean(val) {
				queryBuilder.Where(fmt.Sprintf("%s = ?", attr), val)
			} else {
				queryBuilder.Where(fmt.Sprintf("LOWER(%s) LIKE LOWER(?)", attr), fmt.Sprintf("%%%s%%", val))
			}
		}

	}

	// Count total item.
	queryBuilder.Count(&totalItems)

	// Add offset, limit, and order into query builder.
	queryBuilder.
		Offset(input.GetOffset()).
		Limit(input.GetLimit()).
		Order(fmt.Sprintf("%s %s", input.GetSort(), input.GetOrder()))

	return queryBuilder, totalItems, nil
}

// Pagination is a helper function for get the data of model with pagination.
// It cannot handle Gorm Preload.
func Pagination(input *models.PaginationInput, model interface{}) ([]map[string]interface{}, *models.ResponsePagination, error) {
	// Initialize variables.
	var totalItems int64
	var err error
	var result []map[string]interface{}
	response := &models.ResponsePagination{}

	// Create the query.
	queryBuilder, totalItems, err := PaginationQuery(input, model)
	if err != nil {
		return nil, response, err
	}

	// Query the database.
	err = queryBuilder.Find(&result).Error
	if err != nil {
		return nil, response, err
	}

	// Add pagination detail to response.
	response.Pagination(input, len(result), int(totalItems))

	return result, response, err
}
