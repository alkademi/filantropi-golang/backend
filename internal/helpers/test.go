package helpers

import (
	"filantropi/internal/models"
	"filantropi/internal/models/enums"
	"time"
)

type TestUserData struct {
	Token string
	User  models.User
}

func InitTestUserData() TestUserData {
	username := "tester"
	return TestUserData{
		User: models.User{
			ID:          42,
			Name:        "Tester",
			Email:       "tester@test.com",
			PhoneNumber: "088812345678",
			Photo:       "",
			Username:    username,
			IsVerified:  false,
			Role:        enums.USER_REGULAR,
			Fundraising: nil,
			Donation:    nil,
			CreatedAt:   time.Time{},
			UpdatedAt:   time.Time{},
		},
		Token: JWTAuthService().GenerateToken(username, true),
	}
}

func InitTestAdminData() TestUserData {
	username := "admin"
	return TestUserData{
		User: models.User{
			ID:          68,
			Name:        "Admin",
			Email:       "admin@admin.com",
			PhoneNumber: "088812345678",
			Photo:       "",
			Username:    username,
			IsVerified:  true,
			Role:        enums.USER_ADMIN,
			Fundraising: nil,
			Donation:    nil,
			CreatedAt:   time.Time{},
			UpdatedAt:   time.Time{},
		},
		Token: JWTAuthService().GenerateToken(username, false),
	}
}
