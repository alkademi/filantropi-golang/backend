package helpers

import (
	"golang.org/x/crypto/bcrypt"
)

// HashPassword is a helper function for hashing the password.
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// CheckHashPassword is a helper function for checking if password and hash is correct.
func CheckHashPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
