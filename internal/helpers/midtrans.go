package helpers

import (
	"crypto/sha512"
	"encoding/hex"
	"filantropi/configs"
	"filantropi/internal/models"
	"github.com/midtrans/midtrans-go"
	"github.com/midtrans/midtrans-go/snap"
	"os"
)

// MidtransHelper is an interface for Midtrans userService.
type MidtransHelper struct {
	Snap snap.Client
}

// SetupGlobalMidtransConfig configure the midtrans globally.
func (*MidtransHelper) SetupGlobalMidtransConfig() {
	midtrans.ServerKey = os.Getenv("MIDTRANS_SERVER_KEY")
	midtrans.Environment = midtrans.Sandbox

	// Optional : here is how if you want to set append payment notification globally
	//midtrans.SetPaymentAppendNotification("https://example.com/append")
	// Optional : here is how if you want to set override payment notification globally
	//midtrans.SetPaymentOverrideNotification("https://example.com/override")

	//// remove the comment bellow, in cases you need to change the default for Log Level
	//midtrans.DefaultLoggerLevel = &midtrans.LoggerImplementation{
	//	LogLevel: midtrans.LogError,
	//}
}

// InitializeSnapClient is a method to initialize the snap client.
func (v *MidtransHelper) InitializeSnapClient() {
	v.Snap.New(os.Getenv("MIDTRANS_SERVER_KEY"), midtrans.Sandbox)
}

// SnapRequestDonation is a method of MidtransHelper to create a snap request for Donation userService.
func (*MidtransHelper) SnapRequestDonation(user models.User,
	donation models.Donation) *snap.Request {
	// Split the user's name to get first name and last name.
	firstName, lastName := SplitName(user.Name)

	// Initiate Snap Request
	snapReq := &snap.Request{
		TransactionDetails: midtrans.TransactionDetails{
			OrderID:  configs.MIDTRANS_ORDER_PREFIX + donation.ID,
			GrossAmt: int64(donation.Amount),
		},
		CreditCard: &snap.CreditCardDetails{
			Secure: true,
		},
		CustomerDetail: &midtrans.CustomerDetails{
			FName: firstName,
			LName: lastName,
			Phone: user.PhoneNumber,
			Email: user.Email,
		},
		EnabledPayments: snap.AllSnapPaymentType,
		Items: &[]midtrans.ItemDetails{
			{
				ID:    "DONATION",
				Price: int64(donation.Amount),
				Qty:   1,
				Name:  "Donation to Filantropi",
			},
		},
	}
	return snapReq
}

// GenerateMidtransSignatureKey is a function for generate signature key.
func GenerateMidtransSignatureKey(orderId string, statusCode string, grossAmount string) string {
	serverKey := os.Getenv("MIDTRANS_SERVER_KEY")

	// Create a hash-key
	hashKey := []byte(orderId + statusCode + grossAmount + serverKey)
	hashValueBytes := sha512.Sum512(hashKey)
	hashValueString := hex.EncodeToString(hashValueBytes[:])
	return hashValueString
}
