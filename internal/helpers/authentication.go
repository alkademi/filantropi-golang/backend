package helpers

import (
	"errors"
	"filantropi/configs"
	"filantropi/internal/models"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"

	"github.com/dgrijalva/jwt-go"
)

type JWTService interface {
	GenerateToken(username string, isUser bool) string
	ValidateToken(token string) (*jwt.Token, error)
}

type authCustomClaims struct {
	Username string
	User     bool
	jwt.StandardClaims
}

type jwtServices struct {
	jwtkey string
	issure string
}

func JWTAuthService() JWTService {
	return &jwtServices{
		jwtkey: os.Getenv("JWT_KEY"),
		issure: "Server",
	}
}

func (service *jwtServices) GenerateToken(username string, isUser bool) string {
	claims := &authCustomClaims{
		username,
		isUser,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * configs.JWT_VALID_TIME).Unix(),
			Issuer:    service.issure,
			IssuedAt:  time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Encoded String
	t, err := token.SignedString([]byte(service.jwtkey))
	if err != nil {
		panic(err)
	}

	return t
}

func (service *jwtServices) ValidateToken(encodedToken string) (*jwt.Token, error) {
	return jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		if _, isvalid := token.Method.(*jwt.SigningMethodHMAC); !isvalid {
			return nil, fmt.Errorf("Invalid Token")
		}
		return []byte(service.jwtkey), nil
	})
}

// parseAuthorizationHeader is a helper function for parsing authentication header with bearer token schema.
// It will return the token as a string.
func parseAuthorizationHeader(c *gin.Context) (string, *models.Response, error) {
	response := &models.Response{}

	// Validate authorization header exist.
	authHeader := c.GetHeader("Authorization")
	if authHeader == "" {
		response.Unauthorized("HTTP Authorization Header is missing", "", c)
		return "", response, errors.New(response.Message)
	}

	// Validate bearer schema is valid.
	authValue := strings.Split(authHeader, " ")
	if len(authHeader) < 2 {
		response.Unauthorized("Authorization Bearer Schema is not valid", "", c)
		return "", response, errors.New(response.Message)
	}

	// Parse the value into variable.
	_, authToken := authValue[0], authValue[1]

	return authToken, response, nil
}

// AuthorizationHeaderValidator is helper function for authenticated the user.
// Parse authentication header with bearer token schema.
// Return user's Username as a string.
func AuthorizationHeaderValidator(c *gin.Context) (string, *models.Response, error) {
	response := &models.Response{}

	// Parse the authorization header
	authToken, response, err := parseAuthorizationHeader(c)
	if err != nil {
		return "", response, err
	}

	// Validate the authToken.
	token, err := JWTAuthService().ValidateToken(authToken)
	if err != nil {
		response.Unauthorized("", "", c)
		return "", response, err
	}

	// Get the claims.
	claims := token.Claims.(jwt.MapClaims)
	username := claims["Username"].(string)

	// Check if username is empty.
	if username == "" {
		response.InternalServerError(
			"Failed to get token claims",
			"token",
			"Cannot parse Username")
		return "", response, err
	}

	return username, response, nil
}

// AuthenticateUser is a helper function for authenticate the user identity.
// It will parse the user from *gin.Context into *models.User.
func AuthenticateUser(c *gin.Context) (*models.User, *models.Response, error) {
	response := &models.Response{}

	// Get the User from context.
	userInterface, exist := c.Get(configs.CONTEXT_USER)
	if !exist {
		response.Unauthorized("User data is missing from context", "", c)
		c.JSON(response.StatusCode, response)
		return nil, response, errors.New(response.Message)
	}

	// Convert the interface user into user.
	user := userInterface.(*models.User)

	return user, response, nil
}
