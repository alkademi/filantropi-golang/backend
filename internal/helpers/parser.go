package helpers

import (
	"github.com/gin-gonic/gin"
)

// ParseRequestBody is a function for passing the request body into map[string]string.
// Remember: The request body only can be parsed once.
func ParseRequestBody(c *gin.Context) (map[string]interface{}, error) {
	// Initialize variable for payload.
	payload := &map[string]interface{}{}

	// Decode req.Body to map[string]interface{}.
	if err := c.ShouldBindJSON(payload); err != nil {
		return nil, err
	}

	return *payload, nil
}
