package helpers

import "strconv"

// isInt is a helper function for checking if a string is an Int.
func isInt(s string) bool {
	_, err := strconv.Atoi(s)
	if err != nil {
		return false
	}
	return true
}

// isBoolean is a helper function for checking if a string is a Boolean.
func isBoolean(s string) bool {
	_, err := strconv.ParseBool(s)
	if err != nil {
		return false
	}
	return true
}
