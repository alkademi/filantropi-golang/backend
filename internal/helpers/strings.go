package helpers

import "strings"

// SplitName is a helper function for splitting the name into firstName and lastName,
// anything between firstName and lastName will be added to the lastName.
func SplitName(name string) (string, string) {
	// Split the name.
	splittedName := strings.Split(name, " ")

	// Get the length of name.
	lenName := len(splittedName)

	switch lenName {
	case 0:
		return "", ""
	case 1:
		return splittedName[0], ""
	case 2:
		return splittedName[0], splittedName[1]
	default:
		return splittedName[0], strings.Join(splittedName[1:], " ")
	}
}
