sleep 10

# Run the seed and server.
go run server.go

# Alternative
# Build the binary.
#go build -o ./bin/binary .

# Run the binary.
#./bin/binary