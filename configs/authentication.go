package configs

const (
	JWT_COOKIE_NAME      = "jwt"
	JWT_VALID_TIME       = 48 // In hours
	AUTHORIZATION_SCHEMA = "Bearer"

	CONTEXT_USER             = "user_id"
	CONTEXT_USER_IS_ADMIN    = "user_is_admin"
	CONTEXT_MIDTRANS_PAYLOAD = "midtrans_payload"
)
