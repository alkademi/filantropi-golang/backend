package configs

import (
	"github.com/gin-gonic/gin"
)

func InitGin() *gin.Engine {
	// Initialize gin.
	router := gin.New()

	// Disable Console Color, you don't need console color when writing the logs to file.
	gin.DisableConsoleColor()

	// Logging to a file.
	//f, _ := os.Init("logs/gin.log")
	//gin.DefaultWriter = io.MultiWriter(f)

	// LoggerWithFormatter middleware will write the logs to gin.DefaultWriter
	// By default gin.DefaultWriter = os.Stdout
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	return router
}
