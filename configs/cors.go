package configs

import (
	"github.com/gin-contrib/cors"
)

func InitCors() cors.Config {
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = []string{"Origin", "Content-Type", "Authorization"}

	return config
}
