package configs

const (
	PAGINATION_PAGE   = 1
	PAGINATION_LIMIT  = 10
	PAGINATION_SORT   = "ID"
	PAGINATION_ORDER  = "asc"
	PAGINATION_SEARCH = ""
)
